FROM php:8.0-apache

RUN apt-get update

RUN docker-php-ext-install mysqli pdo_mysql

RUN apt-get install -y libgd-dev libpng-dev libjpeg-dev \
  && docker-php-ext-configure gd --with-jpeg=/usr/include/ \
  && docker-php-ext-install gd

RUN docker-php-ext-configure exif \
  && docker-php-ext-install exif

RUN apt-get install -y zlib1g-dev libicu-dev g++ \
  && docker-php-ext-configure intl \
  && docker-php-ext-install intl

RUN apt-get install -y libzip-dev \
  && docker-php-ext-install zip

RUN pecl install xdebug-3.0.3 \
  && docker-php-ext-enable xdebug

RUN a2enmod rewrite

#ssl-cert includes /etc/ssl/certs/ssl-cert-snakeoil.pem and /etc/ssl/private/ssl-cert-snakeoil.key
RUN apt-get install -y ssl-cert

RUN a2enmod ssl
RUN a2ensite default-ssl

#create DocumentRoot to avoid apache crashing before volumes are mounted
RUN mkdir -p /var/www/html

RUN usermod -u 1000 www-data

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

ENV COMPOSER_MEMORY_LIMIT=-1

RUN apt-get install -y zip

RUN mkdir -p /var/www/.composer \
  && chown www-data /var/www/.composer \
  && chgrp 1000 /var/www/.composer