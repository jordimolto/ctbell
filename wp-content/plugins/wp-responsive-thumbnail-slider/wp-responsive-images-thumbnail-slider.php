<?php
   /* 
    Plugin Name: WordPress Responsive Thumbnail Slider
    Plugin URI:http://www.i13websolution.com/wordpress-responsive-thumbnail-slider-pro.html 
    Author URI:http://www.i13websolution.com/wordpress-responsive-thumbnail-slider-pro.html
    Description: This is beautiful responsive thumbnail image slider plugin for WordPress.Add any number of images from admin panel.
    Author:I Thirteen Web Solution
    Version:2.3
    */

    add_action('admin_menu', 'add_responsive_thumbnail_slider_admin_menu');
    //add_action( 'admin_init', 'my_responsive_thumbnailSlider_admin_init' );
    register_activation_hook(__FILE__,'install_responsive_thumbnailSlider');
    add_action('wp_enqueue_scripts', 'responsive_thumbnail_slider_load_styles_and_js');
    add_shortcode('print_responsive_thumbnail_slider', 'print_responsive_thumbnail_slider_func' );
    add_filter( 'admin_post_thumbnail_html', 'responsive_thumbnail_slider_add_featured_image_instruction');
    add_filter('widget_text', 'do_shortcode');
    add_action('save_post','responsive_thumbnail_slider_featured_image_save');
    add_action ( 'admin_notices', 'responsive_thumbnail_slider_admin_notices' );
    add_theme_support('post-thumbnails');
    add_action( 'wp_ajax_mass_upload', 'responsive_thumbnail_slider_mass_upload' );
    
    
    
    
    function responsive_thumbnail_slider_load_styles_and_js(){
         if (!is_admin()) {                                                       
             
            wp_enqueue_style( 'images-responsive-thumbnail-slider-style', plugins_url('/css/images-responsive-thumbnail-slider-style.css', __FILE__) );
            wp_enqueue_script('jquery'); 
            wp_enqueue_script('images-responsive-thumbnail-slider-jc',plugins_url('/js/images-responsive-thumbnail-slider-jc.js', __FILE__));
            wp_enqueue_script('effects-eas',plugins_url('/js/effects-eas.js', __FILE__));
            
         }  
      }
      
       function responsive_thumbnail_slider_admin_notices() {
         
	if (is_plugin_active ( 'wp-responsive-thumbnail-slider/wp-responsive-images-thumbnail-slider.php' )) {
		
		$uploads = wp_upload_dir ();
		$baseDir = $uploads ['basedir'];
		$baseDir = str_replace ( "\\", "/", $baseDir );
		$pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
		
		if (file_exists ( $pathToImagesFolder ) and is_dir ( $pathToImagesFolder )) {
			
			if (! is_writable ( $pathToImagesFolder )) {
				echo "<div class='updated'><p>Responsive Thumbnail Slider is active but does not have write permission on</p><p><b>" . $pathToImagesFolder . "</b> directory.Please allow write permission.</p></div> ";
			}
		} else {
			
			wp_mkdir_p ( $pathToImagesFolder );
			if (! file_exists ( $pathToImagesFolder ) and ! is_dir ( $pathToImagesFolder )) {
				echo "<div class='updated'><p>Responsive Thumbnail Slider is active but plugin does not have permission to create directory</p><p><b>" . $pathToImagesFolder . "</b> .Please create wp-responsive-images-thumbnail-slider directory inside upload directory and allow write permission.</p></div> ";
			}
		}
	}
    }    
     function install_responsive_thumbnailSlider(){
           global $wpdb;
           $table_name = $wpdb->prefix . "responsive_thumbnail_slider";
           $table_name2 = $wpdb->prefix . "responsive_thumbnail_slider_settings";
           
                  $sql = "CREATE TABLE " . $table_name . " (
                          `id` int(10)  NOT NULL AUTO_INCREMENT,
                          `title` varchar(1000)  NOT NULL,
                          `image_name` varchar(500)  NOT NULL,
                          `image_description` text  DEFAULT NULL,
                          `image_order` int(11) NOT NULL DEFAULT '0',
                          `open_link_in` tinyint(1) NOT NULL DEFAULT '1',
                          `createdon` datetime NOT NULL,
                          `custom_link` varchar(1000) DEFAULT NULL,
                          `post_id` int(10)  DEFAULT NULL,
                          `slider_id` int(10)  NOT NULL DEFAULT '1',
                           PRIMARY KEY (`id`)
                );
                
                 CREATE TABLE " . $table_name2 . "(
                          `id` int(10)  NOT NULL AUTO_INCREMENT,
                          `name` varchar(200) NOT NULL,
                          `linkimage` tinyint(1) NOT NULL,
                          `pauseonmouseover` tinyint(1) NOT NULL,
                          `auto` tinyint(1) NOT NULL,
                          `speed` int(10)  NOT NULL,
                          `pause` int(10)  NOT NULL,
                          `circular` tinyint(1) NOT NULL,
                          `imageheight` int(10)  NOT NULL,
                          `imagewidth` int(10)  NOT NULL,
                          `imageMargin` int(10)  NOT NULL,
                          `visible` int(10)  NOT NULL,
                          `scroll` int(10)  NOT NULL,
                          `resizeImages` tinyint(1) NOT NULL,
                          `scollerBackground` varchar(10) NOT NULL DEFAULT '#FFFFFF',
                          `easing` varchar(45) DEFAULT NULL,
                          `show_caption` tinyint(1) NOT NULL,
                          `show_pager`  tinyint(1) NOT NULL,
                          `createdon` datetime NOT NULL,
                          `min_visible` int(10)  DEFAULT '1',
                          `is_random` tinyint(1)  DEFAULT '0',
                          `crop_image` tinyint(1)  DEFAULT '1',
                          `bordercolor` varchar(10) NOT NULL DEFAULT '#cccccc',
                          `borderradius` int(10) NOT NULL DEFAULT '5',
                          `boxshadow` varchar(10) NOT NULL DEFAULT '#cccccc',
                           PRIMARY KEY (`id`)
                   );
                
                ";
               require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
               dbDelta($sql);
               
               $uploads = wp_upload_dir ();
               $baseDir = $uploads ['basedir'];
               $baseDir = str_replace ( "\\", "/", $baseDir );
               $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
               wp_mkdir_p ( $pathToImagesFolder );
               
               $row = $wpdb->get_results(  "SELECT is_random FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name2' AND column_name = 'is_random'"  );

                if(empty($row)){
                   $wpdb->query("ALTER TABLE $table_name2 ADD `is_random` tinyint(1) DEFAULT '0' ");
                }
                
               $row = $wpdb->get_results(  "SELECT crop_image FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name2' AND column_name = 'crop_image'"  );

                if(empty($row)){
                   $wpdb->query("ALTER TABLE $table_name2 ADD `crop_image` tinyint(1) DEFAULT '1' ");
                }
                
               $row = $wpdb->get_results(  "SELECT bordercolor FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name2' AND column_name = 'bordercolor'"  );

                if(empty($row)){
                   $wpdb->query("ALTER TABLE $table_name2 ADD `bordercolor` varchar(10) NOT NULL DEFAULT '#cccccc' ");
                }
               
               $row = $wpdb->get_results(  "SELECT borderradius FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name2' AND column_name = 'borderradius' "  );

                if(empty($row)){
                   $wpdb->query("ALTER TABLE $table_name2 ADD `borderradius` int(10) NOT NULL DEFAULT '5' ");
                }
                
               $row = $wpdb->get_results(  "SELECT boxshadow FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name2' AND column_name = 'boxshadow' "  );

                if(empty($row)){
                   $wpdb->query("ALTER TABLE $table_name2 ADD `boxshadow` varchar(10) NOT NULL DEFAULT '#cccccc' ");
                }
               
     } 
    
     function responsive_thumbnail_slider_mass_upload(){
        
      global $wpdb; 
      
      $uploads = wp_upload_dir();
   	  $baseDir=$uploads['basedir'];
	  $baseDir=str_replace("\\","/",$baseDir);
	  $pathToImagesFolder=$baseDir.'/wp-responsive-images-thumbnail-slider/';

      if(isset($_POST) and sizeof($_POST)>0){
      
         if(!check_ajax_referer( 'thumbnail-mass-image','thumbnail_security' )){
          
          wp_die('Security check fail'); 
          
          }  
         $createdOn=date('Y-m-d h:i:s');
         if(function_exists('date_i18n')){
            
             $createdOn=date_i18n('Y-m-d'.' '.get_option('time_format') ,false,false);
            if(get_option('time_format')=='H:i')
                $createdOn=date('Y-m-d H:i:s',strtotime($createdOn));
             else   
               $createdOn=date('Y-m-d h:i:s',strtotime($createdOn));
         } 
         $attachment_id=(int)$_POST['attachment_id'];
         $photoMeta = wp_get_attachment_metadata( $attachment_id );
        
         $open_link_in=0;
         $enable_light_box_img_desc=0;  
         $imageurl='';
         $title=trim(htmlentities(strip_tags($_POST['imagetitle']),ENT_QUOTES));
         $enable_light_box_img_desc=0;     
         $image_description=trim(htmlentities(strip_tags($_POST['image_description']),ENT_QUOTES));
         $sliderid=(int) htmlentities(strip_tags($_POST['slider_id']),ENT_QUOTES);
         
         $orderQ = "select max(image_order) as maxImgOrder from ".$wpdb->prefix."responsive_thumbnail_slider where slider_id=$sliderid";
         $myrow  = $wpdb->get_row($orderQ);
         
         if(is_object($myrow) and isset($myrow->maxImgOrder) and $myrow->maxImgOrder>0){
             
              $image_order=$myrow->maxImgOrder+1;
         }
         else{
              $image_order=0;
         }
         if(is_array($photoMeta) and isset($photoMeta['file'])) {
             
                 $fileName=$photoMeta['file'];
                 $phyPath=ABSPATH;
                 $phyPath=str_replace("\\","/",$phyPath);
               
                 $pathArray=pathinfo($fileName);
               
                 $imagename=$pathArray['basename'];
                 $imagename_=$pathArray['filename'];
                 $file_ext=$pathArray['extension'];
                 $imagename=$imagename_.uniqid().".".$file_ext;
                 $upload_dir_n = wp_upload_dir(); 
                 $upload_dir_n=$upload_dir_n['basedir'];
                 $fileUrl=$upload_dir_n.'/'.$fileName;
                 $fileUrl=str_replace("\\","/",$fileUrl);
                 $wpcurrentdir=dirname(__FILE__);
                 $wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
                 $imageUploadTo=$pathToImagesFolder."/".$imagename;
                 @copy($fileUrl, $imageUploadTo);
                           
          }
      
          $query = "INSERT INTO ".$wpdb->prefix."responsive_thumbnail_slider (title, image_name,image_description,image_order,
                                            open_link_in,createdon,custom_link,slider_id) 
                      VALUES ('$title','$imagename','$image_description',$image_order,$open_link_in,'$createdOn','$imageurl',$sliderid)";
                                   
          $wpdb->query($query); 
         
      }  

   } 
    
    
    
   
    function add_responsive_thumbnail_slider_admin_menu(){
        
        $hook_suffix_r_t_s=add_menu_page( __( 'Responsive Thumbnail Slider'), __( 'Responsive Thumbnail Slider' ), 'administrator', 'responsive_thumbnail_slider', 'responsive_thumbnail_slider_admin_options' );
        $hook_suffix_r_t_s=add_submenu_page( 'responsive_thumbnail_slider', __( 'Slider Setting'), __( 'Manage Sliders' ),'administrator', 'responsive_thumbnail_slider', 'responsive_thumbnail_slider_admin_options' );
        $hook_suffix_r_t_s_1=add_submenu_page( 'responsive_thumbnail_slider', __( 'Manage Images'), __( 'Manage Images'),'administrator', 'responsive_thumbnail_slider_image_management', 'responsive_thumbnail_image_management' );
        $hook_suffix_r_t_s_2=add_submenu_page( 'responsive_thumbnail_slider', __( 'Preview Slider'), __( 'Preview Slider'),'administrator', 'responsive_thumbnail_slider_preview', 'responsivepreviewSliderAdmin' );
        
        add_action( 'load-' . $hook_suffix_r_t_s , 'my_responsive_thumbnailSlider_admin_init' );
        add_action( 'load-' . $hook_suffix_r_t_s_1 , 'my_responsive_thumbnailSlider_admin_init' );
        add_action( 'load-' . $hook_suffix_r_t_s_2 , 'my_responsive_thumbnailSlider_admin_init' );
        
    }
    
    function my_responsive_thumbnailSlider_admin_init(){
      
      $url = plugin_dir_url(__FILE__);  
      
      wp_enqueue_script('jquery.validate', $url.'js/jquery.validate.js' );  
      wp_enqueue_script('images-responsive-thumbnail-slider-jc', $url.'js/images-responsive-thumbnail-slider-jc.js' );  
      wp_enqueue_script('effects-eas',plugins_url('/js/effects-eas.js', __FILE__));
      wp_enqueue_style('images-responsive-thumbnail-slider-style',$url.'css/images-responsive-thumbnail-slider-style.css');
      responsive_thumbnail_slider_admin_scripts_init();
      
    }
    
   
   function responsive_thumbnail_slider_admin_options(){
 
   $action='gridview';
      if(isset($_GET['action']) and $_GET['action']!=''){
        
         $action=trim($_GET['action']);       
      }                    
      if(strtolower($action)==strtolower('gridview')){ 
      
      
      ?>
      <div class="wrap">
        <style type="text/css">
          .pagination {
            clear:both;
            padding:20px 0;
            position:relative;
            font-size:11px;
            line-height:13px;
            }
             
            .pagination span, .pagination a {
            display:block;
            float:left;
            margin: 2px 2px 2px 0;
            padding:6px 9px 5px 9px;
            text-decoration:none;
            width:auto;
            color:#fff;
            background: #555;
            }
             
            .pagination a:hover{
            color:#fff;
            background: #3279BB;
            }
             
            .pagination .current{
            padding:6px 9px 5px 9px;
            background: #3279BB;
            color:#fff;
            }
        </style>
         <!--[if !IE]><!-->
        <style type="text/css">
        
            @media only screen and (max-width: 800px) {
            
            /* Force table to not be like tables anymore */
            #no-more-tables table, 
            #no-more-tables thead, 
            #no-more-tables tbody, 
            #no-more-tables th, 
            #no-more-tables td, 
            #no-more-tables tr { 
                display: block; 
                
            }
         
            /* Hide table headers (but not display: none;, for accessibility) */
            #no-more-tables thead tr { 
                position: absolute;
                top: -9999px;
                left: -9999px;
            }
         
            #no-more-tables tr { border: 1px solid #ccc; }
         
            #no-more-tables td { 
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee; 
                position: relative;
                padding-left: 50%; 
                white-space: normal;
                text-align:left;      
            }
         
            #no-more-tables td:before { 
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%; 
                padding-right: 10px; 
                white-space: nowrap;
                text-align:left;
                font-weight: bold;
            }
         
            /*
            Label the data
            */
            #no-more-tables td:before { content: attr(data-title); }
        }
        </style>
        <!--<![endif]-->
        <?php
            $url = plugin_dir_url(__FILE__);  
        ?> 
       <div id="modelMainDiv" style="display:none;z-index: 1600001; border: medium none; margin: 0pt; padding: 0pt; width: 100%; height: 100%; top: 0pt; left: 0pt; background-color: rgb(0, 0, 0); opacity: 0.2; cursor: wait; position: fixed;filter:alpha(opacity=15)" ></div>
        <div id="LoaderDiv" style="display:none;z-index: 1600002; border: medium none; margin: 0pt; padding: 0pt; width: 100%; height: 100%; top: 0pt; left: 0pt; background-color: rgb(0, 0, 0); opacity: 0.2; cursor: wait; position: fixed;filter:alpha(opacity=15)" ></div>
        <div id="ContainDiv" style="display:none;z-index: 1600003; position: fixed; padding: 5px; margin: 0px; width: 30%; top: 40%; left: 35%; text-align: center; color: rgb(0, 0, 0); border: 1px solid #999999; background-color: rgb(255, 255, 255); cursor: wait;" >
          <img src="<?php echo $url.'images/loading.gif'?>" />
           <h2 id="wait">Please wait while uploading images...</h2>
        </div> 
       <div style="width: 100%;">  
        <div style="float:left;width:100%;" >
       <?php
                
                $messages=get_option('responsive_thumbnail_slider_messages'); 
                $type='';
                $message='';
                if(isset($messages['type']) and $messages['type']!=""){

                $type=$messages['type'];
                $message=$messages['message'];

                }  


                if($type=='err'){ echo "<div class='errMsg'>"; echo $message; echo "</div>";}
                else if($type=='succ'){ echo "<div class='succMsg'>"; echo $message; echo "</div>";}


                update_option('responsive_thumbnail_slider_messages', array());     
          ?>    
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2>Manage Thumbnail Sliders <a class="button add-new-h2" href="admin.php?page=responsive_thumbnail_slider&action=addedit">Add New</a> </h2>
        <br/>    
        <form method="POST" action="admin.php?page=responsive_thumbnail_slider&action=deleteselected"  id="posts-filter">
              <div class="alignleft actions">
                  <select name="action_upper" id="action_upper">
                    <option selected="selected" value="-1">Bulk Actions</option>
                    <option value="delete">delete</option>
                </select>
                <input type="submit" value="Apply" class="button-secondary action" id="deleteselected" name="deleteselected" onclick="return confirmDelete_bulk();">
            </div>
            <div style="clear: both;"></div>
            <br/>
         <div id="no-more-tables">
          <table cellspacing="0" id="gridTbl" class="table-bordered table-striped table-condensed cf" >
             <thead>
                 <tr>
                 
                  <tr>
                    <th class="manage-column column-cb check-column"><input type="checkbox"/></th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Created On</th>
                    <th>Shortcode</th>
                    <th>Manage Images</th>
                    <th>Mass Image Add</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
             </thead>
             
             <tbody id="the-list">
                 <?php
                     global $wpdb;
                     $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider_settings order by createdon desc";
                     $rows=$wpdb->get_results($query);
                     $rowCount=sizeof($rows);

                     if($rowCount > 0){

                         global $wp_rewrite;
                         $rows_per_page = 5;

                         $current = (isset($_GET['paged'])) ? ((int) $_GET['paged']) : 1;
                         $pagination_args = array(
                             'base' => @add_query_arg('paged','%#%'),
                             'format' => '',
                             'total' => ceil(sizeof($rows)/$rows_per_page),
                             'current' => $current,
                             'show_all' => false,
                             'type' => 'plain',
                         );


                         $start = ($current - 1) * $rows_per_page;
                         $end = $start + $rows_per_page;
                         $end = (sizeof($rows) < $end) ? sizeof($rows) : $end;

                         $delRecNonce=wp_create_nonce('delete_slider');
                         for ($i=$start;$i < $end ;++$i ) {
                             $row = $rows[$i];
                             $id=$row->id;
                             $editlink="admin.php?page=responsive_thumbnail_slider&action=addedit&id=$id";
                             $deletelink="admin.php?page=responsive_thumbnail_slider&action=delete&id=$id&nonce=$delRecNonce";
                             $manageImages="admin.php?page=responsive_thumbnail_slider_image_management&sliderid=$id";

                         ?>
                         <tr valign="top"  id="">
                                <td class="alignCenter check-column"   data-title="Select Record" ><input type="checkbox" value="<?php echo $id ?>" name="thumbnails[]"></td>
                                <td class="alignCenter" data-title="Id" ><?php echo $row->id; ?></td>
                                <td class="alignCenter" data-title="Name" ><strong><?php echo $row->name; ?></strong></td>  
                                <td class="alignCenter" data-title="Created On" ><?php echo $row->createdon; ?></td>
                                <td class="alignCenter" data-title="ShortCode"  scope="col"><span><input type="text" spellcheck="false" onclick="this.focus();this.select()" readonly="readonly" style="width: 100%;height: 29px;background-color: #EEEEEE" value='[print_responsive_thumbnail_slider id="<?php echo $row->id; ?>"]'></span></td>
                                <td class="alignCenter" data-title="Mass Images Add" ><strong><a id="<?php echo $id;?>" class="" href='<?php echo $manageImages; ?>' title="Manage Images">Manage Images</a></strong></td>
                                <td class="alignCenter" data-title="Mass Images Add" ><strong><a id="<?php echo $id;?>" class="massAdd" href='<?php echo $manageImages; ?>' title="Mass Images Add">Mass Images Add</a></strong></td>
                                <td class="alignCenter" data-title="Edit" ><strong><a href='<?php echo $editlink; ?>' title="edit">Edit</a></strong></td>  
                                <td class="alignCenter" data-title="Delete" ><strong><a href='<?php echo $deletelink; ?>' onclick="return confirmDelete();"  title="delete">Delete</a> </strong></td>  
                           </tr>
                         <?php 
                         } 
                     }
                     else{
                     ?>
                     <tr valign="top"  id="">
                            <td colspan="9" data-title="No Record" align="center"><strong>No Sliders Found</strong></td>  
                       </tr>
                     <?php 
                     } 
                 ?>      
             </tbody>
         </table>
         <script>
            var $n = jQuery.noConflict();  
            var nonce_sec='<?php echo wp_create_nonce( "thumbnail-mass-image" );?>';
            $n(document).ready(function() {
                   //uploading files variable
                   var custom_file_frame;
                   $n(".massAdd").click(function(event) {
                      var slider_id=$n(this).attr('id'); 
                      event.preventDefault();
                      //If the frame already exists, reopen it
                      if (typeof(custom_file_frame)!=="undefined") {
                         custom_file_frame.close();
                      }

                      //Create WP media frame.
                      custom_file_frame = wp.media.frames.customHeader = wp.media({
                         //Title of media manager frame
                         title: "WP Media Uploader",
                         library: {
                            type: 'image'
                         },
                         button: {
                            //Button text
                            text: "Set Image"
                         },
                         //Do not allow multiple files, if you want multiple, set true
                         multiple: true
                      });

                      //callback for selected image

                      custom_file_frame.on('select', function() {

                        
                            $n("#modelMainDiv").show();
                            $n("#LoaderDiv").show();
                            $n("#ContainDiv").show();
                            var selection = custom_file_frame.state().get('selection');
                            selection.map(function(attachment) {

                                attachment = attachment.toJSON();
                                var validExtensions=new Array();
                                validExtensions[0]='jpg';
                                validExtensions[1]='jpeg';
                                validExtensions[2]='png';
                                validExtensions[3]='gif';
                                

                                var inarr=parseInt($n.inArray( attachment.subtype, validExtensions));

                                if(inarr>0 && attachment.type.toLowerCase()=='image' ){

                                      var titleTouse="";
                                      var imageDescriptionTouse="";

                                      if($n.trim(attachment.title)!=''){

                                         titleTouse=$n.trim(attachment.title); 
                                      }  
                                      else if($n.trim(attachment.caption)!=''){

                                         titleTouse=$n.trim(attachment.caption);  
                                      }

                                      if($n.trim(attachment.description)!=''){

                                         imageDescriptionTouse=$n.trim(attachment.description); 
                                      }  
                                      else if($n.trim(attachment.caption)!=''){

                                         imageDescriptionTouse=$n.trim(attachment.caption);  
                                      }

                                      var data = {
                                                imagetitle:titleTouse,
                                                image_description: imageDescriptionTouse,
                                                attachment_id:attachment.id,
                                                slider_id:slider_id,
                                                action: 'mass_upload',
                                                thumbnail_security:nonce_sec
                                            };

                                        url='admin.php?page=responsive_thumbnail_slider&action=mass_upload'
                                        $n.ajax({
                                              type: 'POST',
                                              url: ajaxurl,
                                              data: data,
                                              success: function(result) {
                                                  if(result.isOk == false)
                                                      alert(result.message);
                                              },
                                              dataType:'html',
                                              async:false
                                            });


                                }  

                            });

                            $n("#modelMainDiv").hide();
                            $n("#LoaderDiv").hide();
                            $n("#ContainDiv").hide();

                        });
                      //Open modal
                      custom_file_frame.open();
                   });
                })
            </script>    
         </div>
  <?php
    if(sizeof($rows)>0){
     echo "<div class='pagination' style='padding-top:10px'>";
     echo paginate_links($pagination_args);
     echo "</div>";
    }
  ?>
    <br/>
    <div class="alignleft actions">
        <select name="action" id="action_bottom">
            <option selected="selected" value="-1">Bulk Actions</option>
            <option value="delete">delete</option>
        </select>
        <input type="submit" value="Apply" class="button-secondary action" id="deleteselected" name="deleteselected" onclick="return confirmDelete_bulk();">
    </div>
       <?php wp_nonce_field('action_settings_mass_delete','mass_delete_nonce'); ?>
    </form>
        <script type="text/JavaScript">

            function  confirmDelete_bulk(){
                var topval=document.getElementById("action_bottom").value;
                var bottomVal=document.getElementById("action_upper").value;

                if(topval=='delete' || bottomVal=='delete'){


                    var agree=confirm("Are you sure you want to delete selected sliders? All images related to selected sliders also removed.");
                    if (agree)
                        return true ;
                    else
                        return false;
                }
            }
            function  confirmDelete(){
            var agree=confirm("Are you sure you want to delete this slider ? All Images related to this slider also removed.");
            if (agree)
                 return true ;
            else
                 return false;
        }
     </script>

        <br class="clear">
        </div>
        <div style="clear: both;"></div>
        <?php $url = plugin_dir_url(__FILE__);  ?>
      </div>  
    </div>  
    <div class="clear"></div> 
     <?php  
      } 
      else if(strtolower($action)==strtolower('addedit')){
     
       $url = plugin_dir_url(__FILE__);
       
       if(isset($_POST['btnsave'])){
          
                  

            if ( !check_admin_referer( 'action_image_add_edit','add_edit_image_nonce')){

               wp_die('Security check fail'); 
            }

            $name=trim(htmlentities(strip_tags($_POST['name']),ENT_QUOTES));
            
            $auto=trim(htmlentities(strip_tags($_POST['isauto']),ENT_QUOTES));

            if($auto=='auto')
                $auto=1;
            else if($auto=='manuall')
                $auto=0;
            else
                $auto=2; 

            $speed=(int)trim(htmlentities(strip_tags($_POST['speed']),ENT_QUOTES));
            $pause=(int)trim(htmlentities(strip_tags($_POST['pause']),ENT_QUOTES));

            if(isset($_POST['circular']))
                $circular=1;  
            else
                $circular=0;  

          
            $visible=trim(htmlentities(strip_tags($_POST['visible']),ENT_QUOTES));
            
            $min_visible=trim(htmlentities(strip_tags($_POST['min_visible']),ENT_QUOTES));


            if(isset($_POST['pauseonmouseover']))
                $pauseonmouseover=1;  
            else 
                $pauseonmouseover=0;

            $easing="";
            if(isset($_POST['easing'])){
                $easing=htmlentities(strip_tags($_POST['easing'],ENT_QUOTES));  
            }

            if(isset($_POST['linkimage']))
                $linkimage=1;  
            else 
                $linkimage=0;
           
            $show_caption=htmlentities(strip_tags($_POST['show_caption'],ENT_QUOTES));  
          
            $show_pager=htmlentities(strip_tags($_POST['show_pager'],ENT_QUOTES));  
          
            $is_random=htmlentities(strip_tags($_POST['is_random'],ENT_QUOTES));  
            $crop_image=htmlentities(strip_tags($_POST['crop_image'],ENT_QUOTES));  
            $bordercolor=htmlentities(strip_tags($_POST['bordercolor'],ENT_QUOTES));  
            $borderradius=htmlentities(strip_tags($_POST['borderradius'],ENT_QUOTES));  
            $boxshadow=htmlentities(strip_tags($_POST['boxshadow'],ENT_QUOTES));  
           
            $scroll=trim(htmlentities(strip_tags($_POST['scroll']),ENT_QUOTES));

            if($scroll=="")
                $scroll=1;

            $imageMargin=(int)trim(htmlentities(strip_tags($_POST['imageMargin']),ENT_QUOTES));    
            $imageheight=(int)trim(htmlentities(strip_tags($_POST['imageheight']),ENT_QUOTES));
            $imagewidth=(int)trim(htmlentities(strip_tags($_POST['imagewidth']),ENT_QUOTES));
            //$resizeImages=(int)trim($_POST['resizeImages']);
            $scollerBackground=trim(htmlentities(strip_tags($_POST['scollerBackground']),ENT_QUOTES));
            $createdOn=date('Y-m-d h:i:s');
            if(function_exists('date_i18n')){
                    
                    $createdOn=date_i18n('Y-m-d'.' '.get_option('time_format') ,false,false);
                    if(get_option('time_format')=='H:i')
                        $createdOn=date('Y-m-d H:i:s',strtotime($createdOn));
                    else   
                        $createdOn=date('Y-m-d h:i:s',strtotime($createdOn));
                        
                }
            
            global $wpdb;
            
            if(isset($_POST['sliderid'])){
                
                  $sliderId=(int)$_POST['sliderid'];
                  $query = "update ".$wpdb->prefix."responsive_thumbnail_slider_settings set name='$name',linkimage=$linkimage,show_pager=$show_pager, 
                                                        show_caption=$show_caption,pauseonmouseover=$pauseonmouseover,auto=$auto,speed=$speed,pause=$pause, 
                                                        circular=$circular,imageheight=$imageheight,imagewidth=$imagewidth,imageMargin=$imageMargin, 
                                                        visible=$visible,min_visible=$min_visible, scroll=$scroll, 
                                                        scollerBackground='$scollerBackground', easing='$easing',is_random=$is_random,
                                                        crop_image='$crop_image',bordercolor='$bordercolor',borderradius='$borderradius',
                                                        boxshadow='$boxshadow'     
                                                        where id=$sliderId";
                  $wpdb->query($query); 
                  $responsive_thumbnail_slider_messages=array();
                  $responsive_thumbnail_slider_messages['type']='succ';
                  $responsive_thumbnail_slider_messages['message']='Slider settings updated successfully.';
                  update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                                                      
                                                        
            }else{

                  $query = "INSERT INTO ".$wpdb->prefix."responsive_thumbnail_slider_settings (name, linkimage, pauseonmouseover, 
                  auto, speed,pause,circular, imageheight, imagewidth,imageMargin, visible, scroll, 
                  resizeImages, scollerBackground, easing,show_caption,show_pager,createdon,min_visible,is_random,crop_image,bordercolor,
                  borderradius,boxshadow) 
                  VALUES ('$name',$linkimage,$pauseonmouseover,$auto,$speed,$pause,$circular,
                  $imageheight,$imagewidth,$imageMargin,$visible,$scroll,1,'$scollerBackground',
                  '$easing',$show_caption,$show_pager,'$createdOn',$min_visible,$is_random,'$crop_image','$bordercolor',
                  '$borderradius','$boxshadow')";
                  
                  
                  $wpdb->query($query); 
                  $responsive_thumbnail_slider_messages=array();
                  $responsive_thumbnail_slider_messages['type']='succ';
                  $responsive_thumbnail_slider_messages['message']='New Slider added successfully.';
                  update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
            }                                                          
              
           
            $location='admin.php?page=responsive_thumbnail_slider';
            echo "<script type='text/javascript'> location.href='$location';</script>";
            exit;
         
     }  
     
     if(isset($_GET['id'])){
         
         global $wpdb;
         $id= $_GET['id'];
         $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider_settings WHERE id=$id";
         $settings  = $wpdb->get_row($query,ARRAY_A);
        
         if(!is_array($settings)){
     
                
                $settings=array();
                $settings['name']="";
                $settings['linkimage']=null;
                $settings['auto']=null;
                $settings['easing']=null;
                $settings['speed']="";
                $settings['pause']="";
                $settings['circular']=null;
                $settings['scollerBackground']="#FFFFFF";
                $settings['visible']="";
                $settings['min_visible']="";
                $settings['scroll']="";
                $settings['pauseonmouseover']=null;
                $settings['imageheight']="";
                $settings['imagewidth']="";
                $settings['resizeImages']=null;
                $settings['show_caption']=null;
                $settings['show_pager']=null;
                $settings['is_random']=null;
                $settings['imageMargin']='15';
                $settings['crop_image']='1';
                $settings['bordercolor']='#cccccc';
                $settings['borderradius']='5';
                $settings['boxshadow']='#cccccc';
          
          }
         
     }else{
         
                $settings=array();
                $settings['name']="";
                $settings['linkimage']=null;
                $settings['auto']=null;
                $settings['easing']=null;
                $settings['speed']="";
                $settings['pause']="";
                $settings['circular']=null;
                $settings['scollerBackground']="#FFFFFF";
                $settings['visible']="";
                $settings['min_visible']="";
                $settings['scroll']="";
                $settings['pauseonmouseover']=null;
                $settings['imageheight']="";
                $settings['imagewidth']="";
                $settings['resizeImages']=null;
                $settings['show_caption']=null;
                $settings['show_pager']=null;
                $settings['is_random']=null;
                $settings['imageMargin']='15';
                $settings['crop_image']='1';
                $settings['bordercolor']='#cccccc';
                $settings['borderradius']='5';
                $settings['boxshadow']='#cccccc';
          
     }
      
      
?>
<div style="width: 100%;">  
        <div style="float:left;width:100%;">
            <div class="wrap">
            
           <?php if(isset($_GET['id'])): ?>
                <h2>Edit Slider</h2>
           <?php else:?>     
                <h2>Add Slider</h2>
           <?php endif;?>
           <br>
            <div id="poststuff">
              <div id="post-body" class="metabox-holder columns-2">
                <div id="post-body-content">
                  <form method="post" action="" id="scrollersettiings" name="scrollersettiings" >
                     <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Slider Name</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="name" size="30" name="name" value="<?php echo $settings['name'];?>" />
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                    <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Link images with url ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="checkbox" id="linkimage" size="30" name="linkimage" value="" <?php if($settings['linkimage']==true){echo "checked='checked'";} ?> style="width:20px;">&nbsp;Add link to image ? 
                                   <div style="clear:both;margin-top:3px">Add link to image? On click user will redirect to url</div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Auto Scroll ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input style="width:20px;" type='radio' <?php if($settings['auto']==1){echo "checked='checked'";}?>  name='isauto' value='auto' >Auto &nbsp;<input style="width:20px;" type='radio' name='isauto' <?php if($settings['auto']==0){echo "checked='checked'";} ?> value='manuall' >Scroll By Left & Right Arrow &nbsp; &nbsp;<input style="width:20px;" type='radio' name='isauto' <?php if($settings['auto']==2){echo "checked='checked'";} ?> value='both' >Scroll Auto With Arrow
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Slider Easing Effects</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <select name="easing" id="easing">
                                     <option value="">Deault</option>
                                     <option <?php if($settings['easing']=="easeInQuad"){?> selected="selected" <?php }?> value="easeInQuad">Ease In Quad</option>
                                     <option <?php if($settings['easing']=="easeOutQuad"){?> selected="selected" <?php }?> value="easeOutQuad">Ease Out Quad</option>
                                     <option <?php if($settings['easing']=="easeInOutQuad"){?> selected="selected" <?php }?> value="easeInOutQuad">Ease In Out Quad</option>
                                     <option <?php if($settings['easing']=="easeInExpo"){?> selected="selected" <?php }?> value="easeInExpo">Ease In Expo</option>
                                     <option <?php if($settings['easing']=="easeOutExpo"){?> selected="selected" <?php }?> value="easeOutExpo">Ease Out Expo</option>
                                     <option <?php if($settings['easing']=="easeInOutExpo"){?> selected="selected" <?php }?> value="easeInOutExpo">Ease In Out Expo</option>
                                     <option <?php if($settings['easing']=="easeInBounce"){?> selected="selected" <?php }?> value="easeInBounce">Ease In Bounce</option>
                                     <option <?php if($settings['easing']=="easeOutBounce"){?> selected="selected" <?php }?> value="easeOutBounce">Ease Out Bounce</option>
                                     <option <?php if($settings['easing']=="easeInOutBounce"){?> selected="selected" <?php }?> value="easeInOutBounce">Ease In Out Bounce</option>
                                     <option <?php if($settings['easing']=="easeInElastic"){?> selected="selected" <?php }?> value="easeInElastic">Ease In Elastic</option>
                                     <option <?php if($settings['easing']=="easeOutElastic"){?> selected="selected" <?php }?> value="easeOutElastic">Ease Out Elastic</option>
                                     <option <?php if($settings['easing']=="easeInOutElastic"){?> selected="selected" <?php }?> value="easeInOutElastic">Ease In Out Elastic</option>
                                     <option <?php if($settings['easing']=="easeInBack"){?> selected="selected" <?php }?> value="easeInBack">Ease In Back</option>
                                     <option <?php if($settings['easing']=="easeOutBack"){?> selected="selected" <?php }?> value="easeOutBack">Ease Out Back</option>
                                     <option <?php if($settings['easing']=="easeInOutBack"){?> selected="selected" <?php }?> value="easeInOutBack">Ease In Out Back</option>
                                   </select>
                                   
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                              Select easing effect for slider
                             <div style="clear:both"></div>
                           
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label >Speed</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="speed" size="30" name="speed" value="<?php echo $settings['speed']; ?>" style="width:100px;">
                                      <div style="clear:both;margin-top:3px">Example 1000</div>
                                      <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                           
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label >Pause</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="pause" size="30" name="pause" value="<?php echo $settings['pause']; ?>" style="width:100px;">
                                      <div style="clear:both;margin-top:3px">Example 1000</div>
                                      <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both">The amount of time (in ms) between each auto transition</div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label >Circular Slider ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="checkbox" id="circular" size="30" name="circular" value="" <?php if($settings['circular']==true){echo "checked='checked'";} ?> style="width:20px;">&nbsp;Circular Slider ? 
                                     <div style="clear:both"></div>
                                     <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                           
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Slider Background color</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="scollerBackground" size="30" name="scollerBackground" value="<?php echo $settings['scollerBackground']; ?>" style="width:100px;">
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Border Color</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="bordercolor" size="30" name="bordercolor" value="<?php echo $settings['bordercolor']; ?>" style="width:100px;">
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label >Border Radius Size</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="borderradius" size="30" name="borderradius" value="<?php echo $settings['borderradius']; ?>" style="width:100px;">
                                      <div style="clear:both;margin-top:3px"></div>
                                      <div></div>
                                 </td>
                               </tr>
                             </table>
                           
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Box Shadow Color</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="boxshadow" size="30" name="boxshadow" value="<?php echo $settings['boxshadow']; ?>" style="width:100px;">
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Max Visible</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="visible" size="30" name="visible" value="<?php echo $settings['visible']; ?>" style="width:100px;">
                                   <div style="clear:both">This will decide your slider width automatically</div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             specifies the number of items visible at all times within the slider.
                             <div style="clear:both"></div>
                           
                         </div>
                      </div> 
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Min Visible</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="min_visible" size="30" name="min_visible" value="<?php echo $settings['min_visible']; ?>" style="width:100px;">
                                   <div style="clear:both">This will decide your slider width in responsive layout</div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             The responsive layout decide by slider itself using min visible.
                             <div style="clear:both"></div>
                           
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Scroll</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="scroll" size="30" name="scroll" value="<?php echo $settings['scroll']; ?>" style="width:100px;">
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             You can specify the number of items to scroll when you click the next or prev buttons.
                             <div style="clear:both"></div>
                         </div>
                      </div>
                       <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Pause On Mouse Over ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="checkbox" id="pauseonmouseover" size="30" name="pauseonmouseover" value="" <?php if($settings['pauseonmouseover']==true){echo "checked='checked'";} ?> style="width:20px;">&nbsp;Pause On Mouse Over ? 
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                   
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Image Height</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="imageheight" size="30" name="imageheight" value="<?php echo $settings['imageheight']; ?>" style="width:100px;">
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Image Width</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="imagewidth" size="30" name="imagewidth" value="<?php echo $settings['imagewidth']; ?>" style="width:100px;">
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Image Margin</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="text" id="imageMargin" size="30" name="imageMargin" value="<?php echo $settings['imageMargin']; ?>" style="width:100px;">
                                   <div style="clear:both;padding-top:5px">Gap between two images </div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             
                             <div style="clear:both"></div>
                         </div>
                      </div>
                       <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Crop Image when resize ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input style="width:20px;" type='radio' <?php if($settings['crop_image']==true){echo "checked='checked'";}?>  name='crop_image' value='1' >yes &nbsp;<input style="width:20px;" type='radio' name='crop_image' <?php if($settings['crop_image']==false){echo "checked='checked'";} ?> value='0' >No
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Show Pager ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input style="width:20px;" type='radio' <?php if($settings['show_pager']==true){echo "checked='checked'";}?>  name='show_pager' value='1' >yes &nbsp;<input style="width:20px;" type='radio' name='show_pager' <?php if($settings['show_pager']==false){echo "checked='checked'";} ?> value='0' >No
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Show Caption ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input style="width:20px;" type='radio' <?php if($settings['show_caption']==true){echo "checked='checked'";}?>  name='show_caption' value='1' >yes &nbsp;<input style="width:20px;" type='radio' name='show_caption' <?php if($settings['show_caption']==false){echo "checked='checked'";} ?> value='0' >No
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
               
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Sort By Random ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input style="width:20px;" type='radio' <?php if($settings['is_random']==true){echo "checked='checked'";}?>  name='is_random' value='1' >yes &nbsp;<input style="width:20px;" type='radio' name='is_random' <?php if($settings['is_random']==false){echo "checked='checked'";} ?> value='0' >No
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                      <?php if(isset($_GET['id']) and $_GET['id']>0){ ?> 
                           <input type="hidden" name="sliderid" id="sliderid" value="<?php echo $_GET['id'];?>">
                        <?php
                        } 
                      ?> 
                           
                    <?php wp_nonce_field('action_image_add_edit','add_edit_image_nonce'); ?>        
                    <input type="submit"  name="btnsave" id="btnsave" value="<?php if(isset($_GET['id']) and $_GET['id']>0){ ?>Save Changes<?php } else{?>Add<?php }?>" class="button-primary">&nbsp;&nbsp;<input type="button" name="cancle" id="cancle" value="Cancel" class="button-primary" onclick="location.href='admin.php?page=responsive_thumbnail_slider'">
                        
                 </form> 
                  <script type="text/javascript">
                  
                     var $n = jQuery.noConflict();  
                     $n(document).ready(function() {
                     
                        $n("#scrollersettiings").validate({
                            rules: {
                                    name: {
                                      required:true, 
                                      maxlength:200,
                                      minlength:3,
                                    },
                                      isauto: {
                                      required:true
                                    },speed: {
                                      required:true, 
                                      digits:true, 
                                      maxlength:15
                                    },pause: {
                                      required:true, 
                                      digits:true, 
                                      maxlength:15
                                    },
                                    visible:{
                                        required:true,  
                                        digits:true,
                                        maxlength:15
                                        
                                    }, 
                                    min_visible:{
                                        required:true,  
                                        digits:true,
                                        maxlength:15
                                        
                                    },
                                    scroll:{
                                      required:true,
                                      digits:true,
                                      maxlength:15  
                                    },
                                    scollerBackground:{
                                      required:true,
                                      maxlength:7  
                                    },
                                    /*scrollerwidth:{
                                      required:true,
                                      number:true,
                                      maxlength:15    
                                    },*/imageheight:{
                                      required:true,
                                      digits:true,
                                      maxlength:15    
                                    },
                                    imagewidth:{
                                      required:true,
                                      digits:true,
                                      maxlength:15    
                                    },
                                    borderradius:{
                                      required:true,
                                      digits:true,
                                      maxlength:15    
                                    },
                                    imageMargin:{
                                      required:true,
                                      number:true,
                                      maxlength:15    
                                    },
                                    bordercolor:{
                                       required:true 
                                    },
                                    boxshadow:{
                                       required:true 
                                    }
                                    
                               },
                                 errorClass: "image_error",
                                 errorPlacement: function(error, element) {
                                 error.appendTo( element.next().next());
                             } 
                             

                        })
                        
                        $n('#scollerBackground').wpColorPicker();
                        $n('#boxshadow').wpColorPicker();
                        $n('#bordercolor').wpColorPicker();
                        
                    });
                  
                </script> 

                </div>
          </div>
        </div>  
     </div>      
</div>
<div class="clear"></div></div>  
<?php
      }
     else if(strtolower($action)==strtolower('delete')){
               
         
               $retrieved_nonce = '';
            
                if(isset($_GET['nonce']) and $_GET['nonce']!=''){

                    $retrieved_nonce=$_GET['nonce'];

                }
                if (!wp_verify_nonce($retrieved_nonce, 'delete_slider' ) ){


                    wp_die('Security check fail'); 
                }
                
               global $wpdb;
               $location="admin.php?page=responsive_thumbnail_slider";
               $deleteId=(int) htmlentities(strip_tags($_GET['id'],ENT_QUOTES));
               
               $uploads = wp_upload_dir ();
               $baseDir = $uploads ['basedir'];
               $baseDir = str_replace ( "\\", "/", $baseDir );
               $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
                    
                    try{
                             
                        
                            $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE slider_id=$deleteId";
                            $myrows  = $wpdb->get_results($query);
                            
                            foreach($myrows as $myrow){
                                
                                if(is_object($myrow)){
                                    
                                    $image_name=$myrow->image_name;
                                    //$imagename=$_FILES["image_name"]["name"];
                                    $imagetoDel=$pathToImagesFolder.'/'.$image_name;
                                    @unlink($imagetoDel);
                                                
                                     $query = "delete from  ".$wpdb->prefix."responsive_thumbnail_slider where id=".$myrow->id;
                                     $wpdb->query($query); 
                                     
                                }
                            } 
                            
                           $query = "delete from  ".$wpdb->prefix."responsive_thumbnail_slider_settings where id=$deleteId";
                           $wpdb->query($query); 
                       
                           $responsive_thumbnail_slider_messages=array();
                           $responsive_thumbnail_slider_messages['type']='succ';
                           $responsive_thumbnail_slider_messages['message']='Slider deleted successfully.';
                           update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);    

         
                     }
                   catch(Exception $e){
                   
                          $responsive_thumbnail_slider_messages=array();
                          $responsive_thumbnail_slider_messages['type']='err';
                          $responsive_thumbnail_slider_messages['message']='Error while deleting image.';
                          update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                    }  
                              
              
              echo "<script type='text/javascript'> location.href='$location';</script>";
              exit;
                  
      }  
      else if(strtolower($action)==strtolower('deleteselected')){
          
             if(!check_admin_referer('action_settings_mass_delete','mass_delete_nonce')){
               
                 wp_die('Security check fail'); 
              }
            
               $uploads = wp_upload_dir ();
               $baseDir = $uploads ['basedir'];
               $baseDir = str_replace ( "\\", "/", $baseDir );
               $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
               
               global $wpdb; 
               $location="admin.php?page=responsive_thumbnail_slider";
               if(isset($_POST) and isset($_POST['deleteselected']) and  ( $_POST['action']=='delete' or $_POST['action_upper']=='delete')){
              
                    if(sizeof($_POST['thumbnails']) >0){
                    
                            $deleteto=$_POST['thumbnails'];
                            $implode=implode(',',$deleteto);   
                            
                            try{
                                    
                                   foreach($deleteto as $deleteId){ 
                                       
                                            $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE slider_id=$deleteId";
                                            $myrows  = $wpdb->get_results($query);
                                            
                                            foreach($myrows as $myrow){
                                                
                                                if(is_object($myrow)){
                                                    
                                                    $image_name=$myrow->image_name;
                                                    //$imagename=$_FILES["image_name"]["name"];
                                                    $imagetoDel=$pathToImagesFolder.'/'.$image_name;
                                                    @unlink($imagetoDel);
                                                                
                                                     $query = "delete from  ".$wpdb->prefix."responsive_thumbnail_slider where id=".$myrow->id;
                                                     $wpdb->query($query); 
                                                     
                                                }
                                            } 
                                            
                                          $query = "delete from  ".$wpdb->prefix."responsive_thumbnail_slider_settings where id=$deleteId";
                                          $wpdb->query($query); 
                                      
                                   }
                                   $responsive_thumbnail_slider_messages=array();
                                   $responsive_thumbnail_slider_messages['type']='succ';
                                   $responsive_thumbnail_slider_messages['message']='Selected sliders deleted successfully.';
                                   update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);    
                 
                             }
                           catch(Exception $e){
                           
                                  $responsive_thumbnail_slider_messages=array();
                                  $responsive_thumbnail_slider_messages['type']='err';
                                  $responsive_thumbnail_slider_messages['message']='Error while deleting image.';
                                  update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                            }  
                                  
                           
                           echo "<script type='text/javascript'> location.href='$location';</script>";
                           exit;
                    
                    
                    }
                    else{
                    
                        
                        echo "<script type='text/javascript'> location.href='$location';</script>";   
                        exit;
                    }
                
               }
               else{
                     
                     echo "<script type='text/javascript'> location.href='$location';</script>";      
                     exit;
               }
         
          }      
      
 
   } 
   
   function responsive_thumbnail_image_management(){
    
      $action='gridview';
      global $wpdb;
      
      $sliderId=0;
      if(isset($_GET['sliderid']) and $_GET['sliderid']>0){
          //do nothing
          
          $sliderId=(int)(trim(htmlentities(strip_tags($_GET['sliderid']),ENT_QUOTES)));
       
      }
      else{
          
            
            $responsive_thumbnail_slider_messages=array();
            $responsive_thumbnail_slider_messages['type']='err';
            $responsive_thumbnail_slider_messages['message']='Please select slider. Click on "Manage Images" of your desired slider.';
            update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
            $location='admin.php?page=responsive_thumbnail_slider';
            echo "<script type='text/javascript'> location.href='$location';</script>";
            exit;
            
      }
      
      if(isset($_GET['action']) and $_GET['action']!=''){
         
   
         $action=trim($_GET['action']);
       }
       
      if(strtolower($action)==strtolower('gridview')){ 
      
          
          $wpcurrentdir=dirname(__FILE__);
          $wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
          
      
   ?> 
    <div class="wrap">
          <!--[if !IE]><!-->
          <style type="text/css">
              @media only screen and (max-width: 800px) {

                  /* Force table to not be like tables anymore */
                  #no-more-tables table, 
                  #no-more-tables thead, 
                  #no-more-tables tbody, 
                  #no-more-tables th, 
                  #no-more-tables td, 
                  #no-more-tables tr { 
                      display: block; 

                  }

                  /* Hide table headers (but not display: none;, for accessibility) */
                  #no-more-tables thead tr { 
                      position: absolute;
                      top: -9999px;
                      left: -9999px;
                  }

                  #no-more-tables tr { border: 1px solid #ccc; }

                  #no-more-tables td { 
                      /* Behave  like a "row" */
                      border: none;
                      border-bottom: 1px solid #eee; 
                      position: relative;
                      padding-left: 50%; 
                      white-space: normal;
                      text-align:left;      
                  }

                  #no-more-tables td:before { 
                      /* Now like a table header */
                      position: absolute;
                      /* Top/left values mimic padding */
                      top: 6px;
                      left: 6px;
                      width: 45%; 
                      padding-right: 10px; 
                      white-space: nowrap;
                      text-align:left;
                      font-weight: bold;
                  }

                  /*
                  Label the data
                  */
                  #no-more-tables td:before { content: attr(data-title); }
              }
          </style>
          <!--<![endif]-->
          <style type="text/css">
          .pagination {
            clear:both;
            padding:20px 0;
            position:relative;
            font-size:11px;
            line-height:13px;
            }
             
            .pagination span, .pagination a {
            display:block;
            float:left;
            margin: 2px 2px 2px 0;
            padding:6px 9px 5px 9px;
            text-decoration:none;
            width:auto;
            color:#fff;
            background: #555;
            }
             
            .pagination a:hover{
            color:#fff;
            background: #3279BB;
            }
             
            .pagination .current{
            padding:6px 9px 5px 9px;
            background: #3279BB;
            color:#fff;
            }
        </style>
        <?php 
             
             $messages=get_option('responsive_thumbnail_slider_messages'); 
             $type='';
             $message='';
             if(isset($messages['type']) and $messages['type']!=""){
             
                $type=$messages['type'];
                $message=$messages['message'];
                
             }  
             
  
             if($type=='err'){ echo "<div class='errMsg'>"; echo $message; echo "</div>";}
             else if($type=='succ'){ echo "<div class='succMsg'>"; echo $message; echo "</div>";}
             
             
             update_option('responsive_thumbnail_slider_messages', array());  
             
             $uploads = wp_upload_dir ();
             $baseDir = $uploads ['basedir'];
             $baseDir = str_replace ( "\\", "/", $baseDir );

             $baseurl=$uploads['baseurl'];
             $baseurl.='/wp-responsive-images-thumbnail-slider/';
             $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
        ?>
       <div style="width: 100%;">  
        <div style="float:left;width:100%;" >
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2>Images <a class="button add-new-h2" href="admin.php?page=responsive_thumbnail_slider_image_management&action=addedit&sliderid=<?php echo $sliderId;?>">Add New</a> </h2>
        <br/>    
        <form method="POST" action="admin.php?page=responsive_thumbnail_slider_image_management&action=deleteselected&sliderid=<?php echo $sliderId;?>"  id="posts-filter">
              <div class="alignleft actions">
                  <select name="action_upper" id="action_upper">
                    <option selected="selected" value="-1">Bulk Actions</option>
                    <option value="delete">delete</option>
                </select>
                <input type="submit" value="Apply" class="button-secondary action" id="deleteselected" name="deleteselected" onclick="return confirmDelete_bulk();">
            </div>
         <br class="clear">
        <?php 
        
          
          global $wpdb;
          $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider_settings WHERE id=$sliderId";
          $settings  = $wpdb->get_row($query,ARRAY_A);
        
          if(!is_array($settings)){
              
             
             $responsive_thumbnail_slider_messages=array();
             $responsive_thumbnail_slider_messages['type']='err';
             $responsive_thumbnail_slider_messages['message']='No such slider found.';
             update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
             $location='admin.php?page=responsive_thumbnail_slider';
             echo "<script type='text/javascript'> location.href='$location';</script>";
             exit;
     
          }
          
          $visibleImages=$settings['visible'];
          $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider where slider_id=$sliderId order by image_order,createdon desc";
          $rows=$wpdb->get_results($query,'ARRAY_A');
          $rowCount=sizeof($rows);
          
        ?>
        <?php if($rowCount<$visibleImages){ ?>
            <h4 style="color: green"> Current slider setting - Total max visible images <?php echo $visibleImages; ?></h4>
            <h4 style="color: green">Please add atleast <?php echo $visibleImages; ?> images</h4>
        <?php } else{
            echo "<br/>";
        }?>
        <div id="no-more-tables">
            <table cellspacing="0" id="gridTbl" class="table-bordered table-striped table-condensed cf" style="">
            <thead>
            
               <tr>
                 <th style="" class="manage-column column-cb check-column" scope="col"><input type="checkbox"></th>
                 <th style=""  scope="col">Id</th>
                 <th style=""  scope="col"><span>Title</span></th>
                 <th style=""  scope="col"><span></span></th>
                 <th style=""  scope="col"><span>Display Order</span></th>
                 <th style=""   scope="col"><span>Published On</span></th>
                 <th style=""  scope="col"><span>Edit</span></th>
                 <th style=""  scope="col"><span>Delete</span></th>
              </tr>   
              
              </thead>
            <tbody id="the-list">
                <?php

                    if(count($rows) > 0){

                        global $wp_rewrite;
                        $rows_per_page = 5;

                        $current = (isset($_GET['paged'])) ? ((int)$_GET['paged']) : 1;
                        $pagination_args = array(
                            'base' => @add_query_arg('paged','%#%'),
                            'format' => '',
                            'total' => ceil(sizeof($rows)/$rows_per_page),
                            'current' => $current,
                            'show_all' => false,
                            'type' => 'plain',
                        );


                        $start = ($current - 1) * $rows_per_page;
                        $end = $start + $rows_per_page;
                        $end = (sizeof($rows) < $end) ? sizeof($rows) : $end;


                        $delRecNonce=wp_create_nonce('delete_image');
                        for ($i=$start;$i < $end ;++$i ) {

                            $row = $rows[$i];

                            $id=$row['id'];
                            $editlink="admin.php?page=responsive_thumbnail_slider_image_management&action=addedit&id=$id&sliderid=$sliderId";
                            $deletelink="admin.php?page=responsive_thumbnail_slider_image_management&action=delete&id=$id&sliderid=$sliderId&nonce=$delRecNonce";
                            $outputimgmain = $baseurl.$row['image_name']; 
                        ?>
                         <tr valign="top" class="alternate author-self status-publish format-default iedit" id="post-113">
                            <td  data-title="Select Record" class="alignCenter check-column" ><input type="checkbox" value="<?php echo $row['id'] ?>" name="thumbnails[]"></td>
                            <td  data-title="Id" class="alignCenter" ><?php echo $row['id']; ?></td>
                            <td  data-title="Title" class="alignCenter"><strong><?php echo $row['title']; ?></strong></td>  
                            <td  data-title="Image" class="alignCenter">
                              <img src="<?php echo $outputimgmain;?>" style="width:50px" height="50px"/>
                            </td> 
                            <td   data-title="Image Order" class="alignCenter"><?php echo $row['image_order'] ?></td>
                            <td   data-title="Created On" class="alignCenter"><?php echo $row['createdon'] ?></td>
                            <td   data-title="Edit" class="alignCenter"><strong><a href='<?php echo $editlink; ?>' title="edit">Edit</a></strong></td>  
                            <td   data-title="Delete" class="alignCenter"><strong><a href='<?php echo $deletelink; ?>' onclick="return confirmDelete();"  title="delete">Delete</a> </strong></td>  
                       </tr>
                        <?php 
                        } 
                    }
                    else{
                    ?>

                  <tr valign="top"  id="">
                            <td colspan="8" data-title="No Record" align="center"><strong>No Images Found</strong></td>  
                   </tr>
                    <?php 
                    } 
                ?>      
            </tbody>
        </table>
        </div>
  <?php
    if(sizeof($rows)>0){
     echo "<div class='pagination' style='padding-top:10px'>";
     echo paginate_links($pagination_args);
     echo "</div>";
    }
  ?>
    <br/>
    <div class="alignleft actions">
        <select name="action" id="action_bottom">
            <option selected="selected" value="-1">Bulk Actions</option>
            <option value="delete">delete</option>
        </select>
        
        <?php wp_nonce_field('action_settings_mass_delete','mass_delete_nonce'); ?>
        <input type="submit" value="Apply" class="button-secondary action" id="deleteselected" name="deleteselected" onclick="return confirmDelete_bulk();">
    </div>

    </form>
        <script type="text/JavaScript">

            function  confirmDelete_bulk(){
                var topval=document.getElementById("action_bottom").value;
                var bottomVal=document.getElementById("action_upper").value;

                if(topval=='delete' || bottomVal=='delete'){


                    var agree=confirm("Are you sure you want to delete selected images?");
                    if (agree)
                        return true ;
                    else
                        return false;
                }
            }
            
            function  confirmDelete(){
            var agree=confirm("Are you sure you want to delete this image ?");
            if (agree)
                 return true ;
            else
                 return false;
        }
     </script>

        <br class="clear">
        </div>
        <div style="clear: both;"></div>
        <?php $url = plugin_dir_url(__FILE__);  ?>
      </div>  
    </div>  
    
    
    <h3>To print this slider into WordPress Post/Page use below code</h3>
        <input type="text" value='[print_responsive_thumbnail_slider id="<?php echo $sliderId;?>"] ' style="width: 400px;height: 30px" onclick="this.focus();this.select()" />
        <div class="clear"></div>
        <h3>To print this slider into WordPress theme/template PHP files use below code</h3>
        <?php
            $shortcode='[print_responsive_thumbnail_slider id="'.$sliderId.'"]';
        ?>
        <input type="text" value="&lt;?php echo do_shortcode('<?php echo htmlentities($shortcode, ENT_QUOTES); ?>'); ?&gt;" style="width: 400px;height: 30px" onclick="this.focus();this.select()" />
        
        
    <div class="clear"></div>
<?php 
  }   
  else if(strtolower($action)==strtolower('addedit')){
      $url = plugin_dir_url(__FILE__);
      $sliderid=(int)trim($_GET['sliderid']);
    ?>
    <?php        
    if(isset($_POST['btnsave'])){
       
        
       if ( !check_admin_referer( 'action_image_add_edit','add_edit_image_nonce')){

            wp_die('Security check fail'); 
        }
        
       $uploads = wp_upload_dir();
       $baseDir = $uploads ['basedir'];
       $baseDir = str_replace ( "\\", "/", $baseDir );
       $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider'; 
       //edit save
       if(isset($_POST['imageid'])){
       
              
            //add new
                $location="admin.php?page=responsive_thumbnail_slider_image_management&sliderid=$sliderid";
                $title=trim(htmlentities(strip_tags($_POST['imagetitle']),ENT_QUOTES));
                $imageurl=trim(htmlentities(strip_tags($_POST['imageurl']),ENT_QUOTES));
                $imageid=trim(htmlentities(strip_tags($_POST['imageid']),ENT_QUOTES));
                $image_order=trim(htmlentities(strip_tags($_POST['image_order']),ENT_QUOTES));
                if($image_order=="" or $image_order==null)
                    $image_order=0;
                    
                $image_description=trim(htmlentities(strip_tags($_POST['image_description']),ENT_QUOTES));
                
                if(isset($_POST['open_link_in']))
                  $open_link_in=1;  
                else 
                 $open_link_in=0;

                
                $imagename="";
                if(trim($_POST['HdnMediaSelection'])!=''){
                        
                         $postThumbnailID=(int) trim(htmlentities(strip_tags($_POST['HdnMediaSelection']),ENT_QUOTES));
                         $photoMeta = wp_get_attachment_metadata( $postThumbnailID );
                         if(is_array($photoMeta) and isset($photoMeta['file'])) {
                             
                                 $fileName=$photoMeta['file'];
                                 $phyPath=ABSPATH;
                                 $phyPath=str_replace("\\","/",$phyPath);
                               
                                 $pathArray=pathinfo($fileName);
                               
                                 $imagename=$pathArray['basename'];
                                 $imagename_=$pathArray['filename'];
                                 $file_ext=$pathArray['extension'];
                                 $imagename=$imagename_.uniqid().".".$file_ext;
                                 $upload_dir_n = wp_upload_dir(); 
                                 $upload_dir_n=$upload_dir_n['basedir'];
                                 $fileUrl=$upload_dir_n.'/'.$fileName;
                                 $fileUrl=str_replace("\\","/",$fileUrl);
                               
                                 $wpcurrentdir=dirname(__FILE__);
                                 $wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
                                 $imageUploadTo=$pathToImagesFolder.'/'.$imagename;
                                       
                                @copy($fileUrl, $imageUploadTo);
                                           
                         }
                        
                    }
                     
                        try{
                                if($imagename!=""){
                                    $query = "update ".$wpdb->prefix."responsive_thumbnail_slider set title='$title',image_name='$imagename',
                                              custom_link='$imageurl',image_order=$image_order,open_link_in=$open_link_in,image_description='$image_description' where id=$imageid and slider_id=$sliderid";
                                 }
                                else{
                                     $query = "update ".$wpdb->prefix."responsive_thumbnail_slider set title='$title',
                                               custom_link='$imageurl',image_order=$image_order,open_link_in=$open_link_in,image_description='$image_description'  where id=$imageid and slider_id=$sliderid";
                                } 
                                $wpdb->query($query); 
                               
                                 $responsive_thumbnail_slider_messages=array();
                                 $responsive_thumbnail_slider_messages['type']='succ';
                                 $responsive_thumbnail_slider_messages['message']='image updated successfully.';
                                 update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);

             
                         }
                       catch(Exception $e){
                       
                              $responsive_thumbnail_slider_messages=array();
                              $responsive_thumbnail_slider_messages['type']='err';
                              $responsive_thumbnail_slider_messages['message']='Error while updating image.';
                              update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                        }  
                
              
              echo "<script type='text/javascript'> location.href='$location';</script>";
              exit;
       }
      else{
      
             //add new
                
                $location="admin.php?page=responsive_thumbnail_slider_image_management&sliderid=$sliderid";
                $title=trim(htmlentities(strip_tags($_POST['imagetitle']),ENT_QUOTES));
                $imageurl=trim(htmlentities(strip_tags($_POST['imageurl']),ENT_QUOTES));
                $createdOn=date('Y-m-d h:i:s');
                if(function_exists('date_i18n')){
                    
                    $createdOn=date_i18n('Y-m-d'.' '.get_option('time_format') ,false,false);
                    if(get_option('time_format')=='H:i')
                        $createdOn=date('Y-m-d H:i:s',strtotime($createdOn));
                    else   
                        $createdOn=date('Y-m-d h:i:s',strtotime($createdOn));
                }
                $image_order=trim(htmlentities(strip_tags($_POST['image_order']),ENT_QUOTES));
                if($image_order=="" or $image_order==null)
                    $image_order=0;
                $image_description=trim(htmlentities(strip_tags($_POST['image_description']),ENT_QUOTES));
                
                if(isset($_POST['open_link_in']))
                  $open_link_in=1;  
                else 
                 $open_link_in=0;
                
                try{

                            if(trim($_POST['HdnMediaSelection'])!=''){

                                    $postThumbnailID=(int) htmlentities(strip_tags($_POST['HdnMediaSelection']),ENT_QUOTES);
                                    $photoMeta = wp_get_attachment_metadata( $postThumbnailID );

                                    if(is_array($photoMeta) and isset($photoMeta['file'])) {

                                        $fileName=$photoMeta['file'];
                                        $phyPath=ABSPATH;
                                        $phyPath=str_replace("\\","/",$phyPath);

                                        $pathArray=pathinfo($fileName);

                                        $imagename=$pathArray['basename'];
                                        $imagename_=$pathArray['filename'];
                                        $file_ext=$pathArray['extension'];
                                        $imagename=$imagename_.uniqid().".".$file_ext;
                                        $upload_dir_n = wp_upload_dir(); 
                                        $upload_dir_n=$upload_dir_n['basedir'];
                                        $fileUrl=$upload_dir_n.'/'.$fileName;
                                        $fileUrl=str_replace("\\","/",$fileUrl);


                                        $imageUploadTo=$pathToImagesFolder.'/'.$imagename;

                                        @copy($fileUrl, $imageUploadTo);

                                    }

                            }

                            $query = "INSERT INTO ".$wpdb->prefix."responsive_thumbnail_slider (title, image_name,createdon,custom_link,image_order,open_link_in,image_description,slider_id) 
                                     VALUES ('$title','$imagename','$createdOn','$imageurl',$image_order,$open_link_in,'$image_description',$sliderid)";


                            $wpdb->query($query); 

                            $responsive_thumbnail_slider_messages=array();
                            $responsive_thumbnail_slider_messages['type']='succ';
                            $responsive_thumbnail_slider_messages['message']='New image added successfully.';
                            update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);


                    }
                  catch(Exception $e){

                         $responsive_thumbnail_slider_messages=array();
                         $responsive_thumbnail_slider_messages['type']='err';
                         $responsive_thumbnail_slider_messages['message']='Error while adding image.';
                         update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                   }  
                     
                       
                
                echo "<script type='text/javascript'> location.href='$location';</script>";          
                exit;
            
       } 
        
    }
   else{ 
       
        $uploads = wp_upload_dir ();
        $baseurl=$uploads['baseurl'];
        $baseurl.='/wp-responsive-images-thumbnail-slider/';
        
  ?>
     <div style="width: 100%;">  
        <div style="float:left;width:100%;" >
            <div class="wrap">
          <?php if(isset($_GET['id']) and $_GET['id']>0)
          { 
               
                
                $id= htmlentities(strip_tags($_GET['id']),ENT_QUOTES);
                $sliderid=(int)trim(htmlentities(strip_tags($_GET['sliderid']),ENT_QUOTES));
                $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE id=$id and slider_id=$sliderid" ;
                
                $myrow  = $wpdb->get_row($query);
                
                if(is_object($myrow)){
                
                  $title=$myrow->title;
                  $image_link=$myrow->custom_link;
                  $image_name=$myrow->image_name;
                  $image_order=$myrow->image_order;
                  $image_description=$myrow->image_description;
                  $open_link_in=$myrow->open_link_in;
                  
                }   
              
          ?>
            <h2>Update Image </h2>
          <?php }else{ 
                  
                  $title='';
                  $image_link='';
                  $image_name='';
                  $image_order='';
                  $image_description='';
                  $open_link_in=true;
          
          ?>
          <h2>Add Image </h2>
          <?php } ?>
            
            <br/>
            <div id="poststuff">
              <div id="post-body" class="metabox-holder columns-2">
                <div id="post-body-content">
                  <form method="post" action="" id="addimage" name="addimage" enctype="multipart/form-data" >
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label for="link_name">Upload Image</label></h3>
                         <div class="inside" id="fileuploaddiv">
                              <?php if($image_name!=""){ ?>
                                    <div><b>Current Image : </b><a id="currImg" href="<?php echo $baseurl;?><?php echo $image_name; ?>" target="_new"><?php echo $image_name; ?></a></div>
                              <?php } ?>      
                             <div class="uploader">
                              <br/>
                                <a href="javascript:;" class="niks_media" id="myMediaUploader"><b>Click here to add image</b></a>
                                <input id="HdnMediaSelection" name="HdnMediaSelection" type="hidden" value="" />
                              <br/>
                            </div>  
                            <?php if(responsive_thumbnail_slider_get_wp_version()>=3.5){ ?>
                              <script>
                            var $n = jQuery.noConflict();  
                            $n(document).ready(function() {
                                   //uploading files variable
                                   var custom_file_frame;
                                   $n("#myMediaUploader").click(function(event) {
                                      event.preventDefault();
                                      //If the frame already exists, reopen it
                                      if (typeof(custom_file_frame)!=="undefined") {
                                         custom_file_frame.close();
                                      }
                                 
                                      //Create WP media frame.
                                      custom_file_frame = wp.media.frames.customHeader = wp.media({
                                         //Title of media manager frame
                                         title: "WP Media Uploader",
                                         library: {
                                            type: 'image'
                                         },
                                         button: {
                                            //Button text
                                            text: "Set Image"
                                         },
                                         //Do not allow multiple files, if you want multiple, set true
                                         multiple: false
                                      });
                                 
                                      //callback for selected image
                                      custom_file_frame.on('select', function() {
                                         
                                          var attachment = custom_file_frame.state().get('selection').first().toJSON();
                                          
                                           var validExtensions=new Array();
                                            validExtensions[0]='jpg';
                                            validExtensions[1]='jpeg';
                                            validExtensions[2]='png';
                                            validExtensions[3]='gif';
                                           
                                                        
                                            var inarr=parseInt($n.inArray( attachment.subtype, validExtensions));
                                
                                            if(inarr>0 && attachment.type.toLowerCase()=='image' ){
                                                
                                                  var titleTouse="";
                                                  var imageDescriptionTouse="";
                                                  
                                                  if($n.trim(attachment.title)!=''){
                                                      
                                                     titleTouse=$n.trim(attachment.title); 
                                                  }  
                                                  else if($n.trim(attachment.caption)!=''){
                                                     
                                                     titleTouse=$n.trim(attachment.caption);  
                                                  }
                                                  
                                                  if($n.trim(attachment.description)!=''){
                                                      
                                                     imageDescriptionTouse=$n.trim(attachment.description); 
                                                  }  
                                                  else if($n.trim(attachment.caption)!=''){
                                                     
                                                     imageDescriptionTouse=$n.trim(attachment.caption);  
                                                  }
                                                  
                                                $n("#imagetitle").val(titleTouse);  
                                                $n("#image_description").val(imageDescriptionTouse);  
                                                
                                                if(attachment.id!=''){
                                                    $n("#HdnMediaSelection").val(attachment.id);  
                                                }   
                                                
                                            }  
                                            else{
                                                
                                                alert('Invalid image selection.');
                                            }  
                                             //do something with attachment variable, for example attachment.filename
                                             //Object:
                                             //attachment.alt - image alt
                                             //attachment.author - author id
                                             //attachment.caption
                                             //attachment.dateFormatted - date of image uploaded
                                             //attachment.description
                                             //attachment.editLink - edit link of media
                                             //attachment.filename
                                             //attachment.height
                                             //attachment.icon - don't know WTF?))
                                             //attachment.id - id of attachment
                                             //attachment.link - public link of attachment, for example ""http://site.com/?attachment_id=115""
                                             //attachment.menuOrder
                                             //attachment.mime - mime type, for example image/jpeg"
                                             //attachment.name - name of attachment file, for example "my-image"
                                             //attachment.status - usual is "inherit"
                                             //attachment.subtype - "jpeg" if is "jpg"
                                             //attachment.title
                                             //attachment.type - "image"
                                             //attachment.uploadedTo
                                             //attachment.url - http url of image, for example "http://site.com/wp-content/uploads/2012/12/my-image.jpg"
                                             //attachment.width
                                      });
                                 
                                      //Open modal
                                      custom_file_frame.open();
                                   });
                                })
                            </script>
                            <?php } ?> 
                         </div>
                       </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label for="link_name">Image Title</label></h3>
                        <div class="inside">
                            <input type="text" id="imagetitle"   size="30" name="imagetitle" value="<?php echo $title;?>">
                             <div style="clear:both"></div>
                             <div></div>
                             <div style="clear:both"></div>
                            <p><?php _e('Used for slider image caption and title tag for seo'); ?></p>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label for="link_name">Image Url(<?php _e('On click redirect to this url.'); ?>)</label></h3>
                        <div class="inside">
                            <input type="text" id="imageurl" class="url"   size="30" name="imageurl" value="<?php echo $image_link; ?>">
                             <div style="clear:both"></div>
                             <div></div>
                             <div style="clear:both"></div>
                            <p><?php _e('On image click users will redirect to this url.'); ?></p>
                         </div>
                      </div>
                      <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label>Open Link in New Tab ?</label></h3>
                        <div class="inside">
                             <table>
                               <tr>
                                 <td>
                                   <input type="checkbox" id="open_link_in" size="30" name="open_link_in" value="" <?php if($open_link_in==true){echo "checked='checked'";} ?> style="width:20px;">&nbsp;Open Link in New Tab ? 
                                   <div style="clear:both"></div>
                                   <div></div>
                                 </td>
                               </tr>
                             </table>
                             <div style="clear:both"></div>
                         </div>
                      </div>
                       <div class="stuffbox" id="namediv" style="width:100%;">
                         <h3><label for="link_name">Image Order(<?php _e('Image order used in slider image display order.'); ?>)</label></h3>
                        <div class="inside">
                             <input type="text" id="image_order" size="30" name="image_order" value="<?php echo $image_order; ?>" style="width:50px;">
                             <div style="clear:both"></div>
                             <div></div>
                             <div style="clear:both"></div>
                         </div>
                        </div>
                     
                       <div class="stuffbox" id="namediv" style="width:100%;">
                           <h3><label for="link_name">Image Description</label>(<span style="font-size: 12px" > <?php _e('If image description set then it will used in anchor element title tag and img element alt tag for seo.'); ?>)</span></h3>
                        <div class="inside">
                             <textarea cols="90" class="" style="width:100%;" rows="3" id="image_description" name="image_description"><?php echo $image_description;?></textarea>
                             <div style="clear:both"></div>
                             <div></div>
                             <div style="clear:both"></div>
                            <p>
                         </div>
                        </div>
                     
                        <?php if(isset($_GET['id']) and $_GET['id']>0){ ?> 
                           <input type="hidden" name="imageid" id="imageid" value="<?php echo $_GET['id'];?>">
                        <?php
                        } 
                        ?>
                       <?php wp_nonce_field('action_image_add_edit','add_edit_image_nonce'); ?>    
                       <input type="submit" onclick="return validateFile();" name="btnsave" id="btnsave" value="Save Changes" class="button-primary">&nbsp;&nbsp;<input type="button" name="cancle" id="cancle" value="Cancel" class="button-primary" onclick="location.href='admin.php?page=responsive_thumbnail_slider_image_management&sliderid=<?php echo $sliderid;?>'">
                                  
                 </form> 
                  <script type="text/javascript">
                  
                     var $n = jQuery.noConflict();  
                     $n(document).ready(function() {
                     
                        $n("#addimage").validate({
                            rules: {
                                    imagetitle: {
                                      required:true, 
                                      maxlength: 200
                                    },imageurl: {
                                      url:true,  
                                      maxlength: 500
                                    },
                                    image_order:{
                                      digits:true, 
                                      maxlength:15
                                    },
                                    image_name:{
                                      isimage:true  
                                    },
                                    image_description:{
                                      maxlength: 200  
                                    }
                               },
                                 errorClass: "image_error",
                                 errorPlacement: function(error, element) {
                                 error.appendTo( element.next().next().next());
                             } 
                             

                        })
                    });
                  
                  function validateFile(){

                        var $n = jQuery.noConflict();  
                        if($n('#currImg').length>0 || $n.trim($n("#HdnMediaSelection").val())!="" ){
                            return true;
                        }
                        else
                            {
                            $n("#err_daynamic").remove();
                            $n("#myMediaUploader").after('<br/><label class="image_error" id="err_daynamic">Please select file.</label>');
                            return false;  
                        } 

                    }
                
                </script> 

                </div>
          </div>
        </div>  
     </div>      
         </div>
    <?php 
    } 
  }  
  else if(strtolower($action)==strtolower('delete')){
  
      
       $retrieved_nonce = '';
            
        if(isset($_GET['nonce']) and $_GET['nonce']!=''){

            $retrieved_nonce=$_GET['nonce'];

        }
        if (!wp_verify_nonce($retrieved_nonce, 'delete_image' ) ){


            wp_die('Security check fail'); 
        }

       $uploads = wp_upload_dir ();
       $baseDir = $uploads ['basedir'];
       $baseDir = str_replace ( "\\", "/", $baseDir );
       $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
        
       $sliderId=0;
       if(isset($_GET['sliderid']) and $_GET['sliderid']>0){
          //do nothing
          
          $sliderId=(int)(trim(htmlentities(strip_tags($_GET['sliderid']),ENT_QUOTES)));
       
       }
       else{
           
            
            $responsive_thumbnail_slider_messages=array();
            $responsive_thumbnail_slider_messages['type']='err';
            $responsive_thumbnail_slider_messages['message']='Please select slider. Click on "Manage Images" of your desired slider.';
            update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
            $location='admin.php?page=responsive_thumbnail_slider';
            echo "<script type='text/javascript'> location.href='$location';</script>";
            exit;
       }
       
        $location="admin.php?page=responsive_thumbnail_slider_image_management&sliderid=$sliderId";
        $deleteId=(int)$_GET['id'];
                
                try{
                         
                    
                        $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE id=$deleteId and slider_id=$sliderId";
                        $myrow  = $wpdb->get_row($query);
                                    
                        if(is_object($myrow)){
                            
                            $image_name=$myrow->image_name;
                            //$imagename=$_FILES["image_name"]["name"];
                            $imagetoDel=$pathToImagesFolder.'/'.$image_name;
                            @unlink($imagetoDel);
                                        
                             $query = "delete from  ".$wpdb->prefix."responsive_thumbnail_slider where id=$deleteId and slider_id=$sliderId ";
                             $wpdb->query($query); 
                           
                             $responsive_thumbnail_slider_messages=array();
                             $responsive_thumbnail_slider_messages['type']='succ';
                             $responsive_thumbnail_slider_messages['message']='Image deleted successfully.';
                             update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                        }    

     
                 }
               catch(Exception $e){
               
                      $responsive_thumbnail_slider_messages=array();
                      $responsive_thumbnail_slider_messages['type']='err';
                      $responsive_thumbnail_slider_messages['message']='Error while deleting image.';
                      update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                }  
                          
          
          echo "<script type='text/javascript'> location.href='$location';</script>";
          exit;
              
  }  
  else if(strtolower($action)==strtolower('deleteselected')){
      
       if(!check_admin_referer('action_settings_mass_delete','mass_delete_nonce')){
               
            wp_die('Security check fail'); 
        }
        $uploads = wp_upload_dir ();
        $baseDir = $uploads ['basedir'];
        $baseDir = str_replace ( "\\", "/", $baseDir );
        $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
          
          $sliderId=0;
          
           if(isset($_GET['sliderid']) and $_GET['sliderid']>0){
              //do nothing
              
              $sliderId=(int)(trim(htmlentities(strip_tags($_GET['sliderid']),ENT_QUOTES)));
           
           }
           else{
               
                
                $responsive_thumbnail_slider_messages=array();
                $responsive_thumbnail_slider_messages['type']='err';
                $responsive_thumbnail_slider_messages['message']='Please select slider. Click on "Manage Images" of your desired slider.';
                update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                $location='admin.php?page=responsive_thumbnail_slider';
                echo "<script type='text/javascript'> location.href='$location';</script>";
                exit;
           }
           
            $location="admin.php?page=responsive_thumbnail_slider_image_management&sliderid=$sliderId";
    
           if(isset($_POST) and isset($_POST['deleteselected']) and  ( $_POST['action']=='delete' or $_POST['action_upper']=='delete')){
          
                if(sizeof($_POST['thumbnails']) >0){
                
                        $deleteto=$_POST['thumbnails'];
                        $implode=implode(',',$deleteto);   
                        
                        try{
                                
                               foreach($deleteto as $img){ 
                                   
                                    $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE id=$img and slider_id=$sliderId";
                                    $myrow  = $wpdb->get_row($query);
                                    
                                    if(is_object($myrow)){
                                        
                                        $image_name=$myrow->image_name;
                                        //$imagename=$_FILES["image_name"]["name"];
                                        $imagetoDel=$pathToImagesFolder.'/'.$image_name;
                                        @unlink($imagetoDel);
                                     
                                        $query = "delete from  ".$wpdb->prefix."responsive_thumbnail_slider where id=$img and slider_id=$sliderId";
                                        $wpdb->query($query); 
                                   
                                        $responsive_thumbnail_slider_messages=array();
                                        $responsive_thumbnail_slider_messages['type']='succ';
                                        $responsive_thumbnail_slider_messages['message']='selected images deleted successfully.';
                                        update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                                   }
                                  
                             }
             
                         }
                       catch(Exception $e){
                       
                              $responsive_thumbnail_slider_messages=array();
                              $responsive_thumbnail_slider_messages['type']='err';
                              $responsive_thumbnail_slider_messages['message']='Error while deleting image.';
                              update_option('responsive_thumbnail_slider_messages', $responsive_thumbnail_slider_messages);
                        }  
                              
                       
                       echo "<script type='text/javascript'> location.href='$location';</script>";
                       exit;
                
                }
                else{
                
                    
                    echo "<script type='text/javascript'> location.href='$location';</script>";   
                    exit;
                }
            
           }
           else{
                 
                 echo "<script type='text/javascript'> location.href='$location';</script>";      
                 exit;
           }
     
      }      
    
  } 
  
function responsivepreviewSliderAdmin(){
        
           global $wpdb;
           $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider_settings order by createdon desc";
           $rows=$wpdb->get_results($query,'ARRAY_A');
        
            $sliderId=0;
           if(isset($_GET['sliderid']) and $_GET['sliderid']>0){
              $sliderId=(int)(trim($_GET['sliderid']));
            }
           
           $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider_settings WHERE id=$sliderId";
           $settings  = $wpdb->get_row($query,ARRAY_A);            
           
           $rand_Numb=uniqid('thumnail_slider');
           $newrand=uniqid('thumnail_slider_');
           $rand_Num_td=uniqid('divSliderMain');
           $rand_var_name=uniqid('rand_');
      
           $location="admin.php?page=responsive_thumbnail_slider_preview&sliderid=";                  
           
           //$wpcurrentdir=dirname(__FILE__);
           //$wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
           //$settings=get_option('thumbnail_slider_settings');    
           
           $uploads = wp_upload_dir();
           $baseDir = $uploads ['basedir'];
           $baseDir = str_replace ( "\\", "/", $baseDir );

           $baseurl=$uploads['baseurl'];
           $baseurl.='/wp-responsive-images-thumbnail-slider/';
           $pathToImagesFolder = $baseDir . '/wp-responsive-images-thumbnail-slider';
           $crop=$settings['crop_image'];
           $imageheight=$settings['imageheight'];
           $imagewidth=$settings['imagewidth'];
                                          
           
     ?>      
     <style type='text/css' >
      #<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport {
          background: none repeat scroll 0 0 <?php echo $settings['scollerBackground']; ?> !important;
          border: 0px none !important;
          box-shadow: 0 0 0 0 !important;
          /*padding:<?php echo $settings['imageMargin'];?>px !important;*/
          
        }
        
             #<?php echo $rand_Numb;?>  <?php if($crop):?> .v-wrap_ <?php else: ?> .v-wrap <?php endif;?>{
        
               
              border:1px solid <?php echo $settings['bordercolor'];?>;
              box-shadow:0 0 5px <?php echo $settings['boxshadow'];?>;
              border-radius:<?php echo $settings['borderradius'];?>px;
             }  
        
            
             
             #<?php echo $rand_Numb;?>  .bx-wrapper img {
                 border-radius:<?php echo $settings['borderradius'];?>px;
             }
             
           <?php if(!$crop):?>  
            #<?php echo $rand_Numb;?>  .bx-caption {
                position: absolute ;
                bottom: 0 ;
                left: 0 ;
                background: #666\9 ;
                background: rgba(80, 80, 80, 0.75) ;
                width: 100% ;
                border-radius:0 0 <?php echo $settings['borderradius'];?>px <?php echo $settings['borderradius'];?>px;
               
                
            }
              #<?php echo $rand_Numb;?>  .v-wrap{padding:0px;}
             #<?php echo $rand_Numb;?>  .v-box{padding:1px;}
             #<?php echo $rand_Numb;?>  .objFit{padding:0px;}
           
          <?php else: ?>
              #<?php echo $rand_Numb;?>  .bx-caption {
                     position: absolute ;
                    bottom: 0 ;
                    left: 0 ;
                    background: #666\9 ;
                    background: rgba(80, 80, 80, 0.75) ;
                    width: 100% ;
                   /* border-radius:0 0 5px 5px;*/
                    width: calc(100% - 11px);
                     bottom: 5px;
                    left: 6px;
                    overflow-wrap: unset;

                    max-height:38px
                    overflow:hidden;
              }
           <?php endif;?> 
      </style>
       <?php
            $wpcurrentdir=dirname(__FILE__);
            $wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
        ?>
       <div style="width: 100%;">  
            <div style="float:left;width:100%;">
                <div class="wrap">
                        <h2>Slider Preview</h2>
                <br/>
                <br/>
                <b>Select Slider :</b>
                <select name="slider" id="slider" onchange="location.href='<?php echo $location;?>'+this.value">
                <option value="" >Select</option>
                    <?php foreach($rows as $row){?>
                       <option <?php if($sliderId==$row['id']){?>selected="selected" <?php } ?>  value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
                    <?php }?>
                </select>
                <?php if(is_array($settings)){?>
                <div id="poststuff">
                  <div id="post-body" class="metabox-holder columns-2">
                    <div id="post-body-content">
                         <div style="clear: both;"></div>
                        <?php $url = plugin_dir_url(__FILE__);  ?>           
                        
                        <div style="width: auto;postion:relative" id="<?php echo $rand_Num_td;?>">
                          <div id="<?php echo $rand_Numb;?>" class="responsiveSlider" style="margin-top: 2px !important;visibility: hidden;">
                         <?php
                              global $wpdb;
                              $imageheight=$settings['imageheight'];
                              $imagewidth=$settings['imagewidth'];
                              
                              if($settings['is_random']){
                        
                                    $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider where slider_id=".$settings['id']." order by rand()";
                                }
                                else{
                                    $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider where slider_id=".$settings['id']." order by image_order";
                                }
                             
                            $rows=$wpdb->get_results($query,'ARRAY_A');
                            
                            if(count($rows) > 0){
                                foreach($rows as $row){
                                    
                                            $imagename=$row['image_name'];
                                            $imageUploadTo=$pathToImagesFolder.'/'.$imagename;
                                            $imageUploadTo=str_replace("\\","/",$imageUploadTo);
                                            $pathinfo=pathinfo($imageUploadTo);
                                            $filenamewithoutextension=$pathinfo['filename'];
                                            $outputimg="";
                                            
                                            
                                            if($settings['resizeImages']==0){
                                                
                                                
                                                $outputimg = $baseurl.$row['image_name']; 
                                               
                                            }
                                            else{
                                                
                                                    if($crop==1){
                                                        $imagetoCheck=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                                        $imagetoCheckLower=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.strtolower($pathinfo['extension']);
                                                    }
                                                    else{
                                                        
                                                        $imagetoCheck=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.$pathinfo['extension'];
                                                        $imagetoCheckLower=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.strtolower($pathinfo['extension']);
                                                        
                                                    }
                                                    if(file_exists($imagetoCheck)){
                                                       if($crop==1){  
                                                            $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                                       }
                                                       else{
                                                           
                                                           $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.$pathinfo['extension'];
                                                       }
                                                    }
                                                    else if(file_exists($imagetoCheckLower)){
                                                        
                                                        if($crop==1){  
                                                             $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.strtolower($pathinfo['extension']);
                                                        }
                                                        else{
                                                            $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.strtolower($pathinfo['extension']);
                                                        }
                                                    }
                                                   else{
                                                         
                                                         if(function_exists('wp_get_image_editor')){
                                                                
                                                                $image = wp_get_image_editor($pathToImagesFolder."/".$row['image_name']); 
                                                                
                                                                $is_crop=true;
                                                                if($crop==0){
                                                                    
                                                                    $is_crop=false;
                                                                }
                                                                if ( ! is_wp_error( $image ) ) {
                                                                    $image->resize( $imagewidth, $imageheight, $is_crop );
                                                                    $image->save( $imagetoCheck );
                                                                    //$outputimg = plugin_dir_url(__FILE__)."imagestoscroll/".$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                                                    
                                                                     if(file_exists($imagetoCheck)){
                                                                            if($crop==1){
                                                                                $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                                                            }
                                                                            else{
                                                                                $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.$pathinfo['extension'];
                                                                            }
                                                                        }
                                                                        else if(file_exists($imagetoCheckLower)){
                                                                            if($crop==1){
                                                                                $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.strtolower($pathinfo['extension']);
                                                                            }
                                                                            else{
                                                                                $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.strtolower($pathinfo['extension']);
                                                                            }
                                                                        }
                                                                }
                                                               else{
                                                                      $outputimg = $baseurl.$row['image_name'];
                                                               }     
                                                            
                                                          }
                                                         
                                                        else{
                                                            
                                                            $outputimg = $baseurl.$row['image_name']; 
                                                        }  
                                                            
                                                          //$url = plugin_dir_url(__FILE__)."imagestoscroll/".$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                                          
                                                   } 
                                            } 
                                          
                                            $title="";
                                            $rowTitle=$row['title'];
                                            $rowTitle=str_replace("'","’",$rowTitle); 
                                            $rowTitle=str_replace('"','”',$rowTitle); 

                                            $rowDescrption=$row['image_description'];
                                            $rowDescrption=str_replace("'","’",$rowDescrption); 
                                            $rowDescrption=str_replace('"','”',$rowDescrption);    

                                            if(trim($rowDescrption)==''){

                                                    $rowDescrption=$rowTitle;
                                                }
                                   
                                 ?>         
                              
                              <?php if($crop):?>
                                 <div class="v-wrap_"> 
                                    <?php if($settings['linkimage']==true){ ?>                                                                                                                                                                                                                                                                                     
                                      <a <?php if($row['open_link_in']==true): ?>target="_blank" <?php endif;?> <?php if($row['custom_link']!=""):?> href="<?php echo $row['custom_link'];?>" <?php endif;?> title="<?php echo $rowDescrption; ?>" ><img src="<?php echo $outputimg; ?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   /></a>
                                    <?php }else{ ?>
                                          <img src="<?php echo $outputimg;?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   />
                                    <?php } ?> 
                                 </div>
                              <?php else:?>
                              <div class="v-wrap" style="max-height:<?php echo $imageheight;?>px">
                                    <article class="v-box" style="max-width:99.3%">
                                       <?php if($settings['linkimage']==true){ ?>                                                                                                                                                                                                                                                                                     
                                            <a <?php if($row['open_link_in']==true): ?>target="_blank" <?php endif;?> <?php if($row['custom_link']!=""):?> href="<?php echo $row['custom_link'];?>" <?php endif;?> title="<?php echo $rowDescrption; ?>" >
                                                <img  class="objFit center" src="<?php echo $outputimg; ?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   />
                                            </a>
                                          <?php }else{ ?>  
                                             <img  class="objFit center" src="<?php echo $outputimg; ?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   />
                                          <?php } ?>    
                                    </article>
                                </div>
                             <?php endif;?>
                           <?php }?>   
                      <?php }?>   
                    </div>
                        </div>
                    
                    <script>
                    var $n = jQuery.noConflict();  
                    $n(document).ready(function(){
                         var <?php echo $rand_var_name;?>=$n('#<?php echo $rand_Num_td;?>').html();   
                         var <?php echo $rand_var_name;?>_slider=$n('#<?php echo $rand_Numb;?>').bxSlider({
                             <?php if($settings['visible']==1 and $settings['min_visible']==1):?>
                               mode:'fade',
                             <?php endif;?>
                               slideWidth: <?php echo $settings['imagewidth'];?>,
                                minSlides: <?php echo $settings['min_visible'];?>,
                                maxSlides: <?php echo $settings['visible'];?>,
                                moveSlides: <?php echo $settings['scroll'];?>,
                                touchEnabled:false,
                                slideMargin:<?php echo $settings['imageMargin'];?>,  
                                speed:<?php echo $settings['speed']; ?>,
                                pause:<?php echo $settings['pause']; ?>,
                                <?php if($settings['pauseonmouseover'] and ($settings['auto']==1 or $settings['auto']==2) ){ ?>
                                  autoHover: true,
                                <?php }else{ if($settings['auto']==1 or $settings['auto']==2){?>   
                                  autoHover:false,
                                <?php }} ?>
                                <?php if($settings['auto']==1):?>
                                 controls:false,
                                <?php else: ?>
                                  controls:true,
                                <?php endif;?>
                                pager:false,
                                useCSS:false,
                                <?php if($settings['auto']==1 or $settings['auto']==2):?>
                                 autoStart:true,
                                 autoDelay:200,
                                 auto:true,       
                                <?php endif;?>
                                <?php if($settings['circular']):?> 
                                 infiniteLoop: true,
                                <?php else: ?>
                                  infiniteLoop: false,
                                <?php endif;?>
                                <?php if($settings['show_caption']):?>
                                  captions:true,  
                                <?php else:?>
                                  captions:false,
                                <?php endif;?>
                                <?php if($settings['show_pager']):?>
                                  pager:true,  
                                <?php else:?>
                                  pager:false,
                                <?php endif;?>
                                easing: '<?php echo ($settings['easing']); ?>',
                                onSliderLoad: function(){
                                    $n("#<?php echo $rand_Numb;?>").css('visibility','visible');
                                    <?php if($crop==0):?>
                                        
                                        //$n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height($n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height()+15);
                                        
                                        $n("#<?php echo $rand_Numb;?> div.v-wrap ").height($n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height());
                                    <?php endif;?>    
                                }
                            
                          });
                          
                         
    
                          
                            <?php if($crop==0):?>
                                  var timer;
                                  var width = $n(window).width();
                                    $n(window).bind('resize', function(){
                                        if($n(window).width() != width){
                                            
                                            $n("#<?php echo $rand_Numb;?> div.v-wrap ").height('');
                                            width = $n(window).width();
                                            timer && clearTimeout(timer);
                                            timer = setTimeout(onResize<?php echo $newrand;?>, 1200);
                                            
                                        }   
                                    });
                              <?php endif;?>     
                               
                             
                                function onResize<?php echo $newrand;?>(){
                                
                                        var currentslide=<?php echo $rand_var_name;?>_slider.getCurrentSlide();

                                      // <?php echo $rand_var_name;?>_slider.reloadSlider();

                                      

                                       setTimeout(function(){ 

                                            $n("#<?php echo $rand_Numb;?> div.v-wrap ").height($n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height());
                                          //  <?php echo $rand_var_name;?>_slider.goToSlide(currentslide);


                                        }, 200);
                                        
                                      


                                }
                                  
                                
                            
                    });         
                    
                     </script>         
                        
                    </div>
              </div>
            </div>  
                <?php }?>
         </div>      
    </div>                                      
    <div class="clear"></div>
    </div>
    <?php if(is_array($settings)){?>
    
        <h3>To print this slider into WordPress Post/Page use below code</h3>
        <input type="text" value='[print_responsive_thumbnail_slider id="<?php echo $sliderId;?>"] ' style="width: 400px;height: 30px" onclick="this.focus();this.select()" />
        <div class="clear"></div>
        <h3>To print this slider into WordPress theme/template PHP files use below code</h3>
        <?php
            $shortcode='[print_responsive_thumbnail_slider id="'.$sliderId.'"]';
        ?>
        <input type="text" value="&lt;?php echo do_shortcode('<?php echo htmlentities($shortcode, ENT_QUOTES); ?>'); ?&gt;" style="width: 400px;height: 30px" onclick="this.focus();this.select()" />
       
    <?php } ?>
    <div class="clear"></div>
<?php       
   }    
   
function print_responsive_thumbnail_slider_func($atts){
       
       global $wpdb;
       extract( shortcode_atts( array('id' => 0,), $atts ) );
       $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider_settings WHERE id=$id";
       $settings  = $wpdb->get_row($query,ARRAY_A);            
       $rand_Numb=uniqid('thumnail_slider');
       $rand_Num_td=uniqid('divSliderMain');
       $rand_var_name=uniqid('rand_');
       $wpcurrentdir=dirname(__FILE__);
       $wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
       
       
       $uploads = wp_upload_dir();
       $baseurl=$uploads['baseurl'];
       $baseurl.='/wp-responsive-images-thumbnail-slider/';
       $baseDir=$uploads['basedir'];
       $baseDir=str_replace("\\","/",$baseDir);
       $pathToImagesFolder=$baseDir.'/wp-responsive-images-thumbnail-slider';
       $crop=$settings['crop_image'];
       $newrand=uniqid('rand_');
       
       $imageheight=$settings['imageheight'];
       $imagewidth=$settings['imagewidth'];
       ob_start(); 
       
 ?><?php $url = plugin_dir_url(__FILE__);  ?><!-- print_responsive_thumbnail_slider_func --><style type='text/css' >
        #<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport {
          background: none repeat scroll 0 0 <?php echo $settings['scollerBackground']; ?> !important;
          border: 0px none !important;
          box-shadow: 0 0 0 0 !important;
          /*padding:<?php echo $settings['imageMargin'];?>px !important;*/
        }
        
          #<?php echo $rand_Numb;?>  <?php if($crop):?> .v-wrap_ <?php else: ?> .v-wrap <?php endif;?>{
        
        
               
              border:1px solid <?php echo $settings['bordercolor'];?>;
              box-shadow:0 0 5px <?php echo $settings['boxshadow'];?>;
              border-radius:<?php echo $settings['borderradius'];?>px;
             }  
        
            
             
             #<?php echo $rand_Numb;?>  .bx-wrapper img {
                 border-radius:<?php echo $settings['borderradius'];?>px;
             }
             
           <?php if(!$crop):?>  
            #<?php echo $rand_Numb;?>  .bx-caption {
                position: absolute ;
                bottom: 0 ;
                left: 0 ;
                background: #666\9 ;
                background: rgba(80, 80, 80, 0.75) ;
                width: 100% ;
                border-radius:0 0 <?php echo $settings['borderradius'];?>px <?php echo $settings['borderradius'];?>px;
               
                
            }
             #<?php echo $rand_Numb;?>  .v-wrap{padding:0px;}
             #<?php echo $rand_Numb;?>  .v-box{padding:1px;}
             #<?php echo $rand_Numb;?>  .objFit{padding:0px;}
             
           <?php else: ?>
              #<?php echo $rand_Numb;?>  .bx-caption {
                     position: absolute ;
                    bottom: 0 ;
                    left: 0 ;
                    background: #666\9 ;
                    background: rgba(80, 80, 80, 0.75) ;
                    width: 100% ;
                   /* border-radius:0 0 5px 5px;*/
                    width: calc(100% - 11px);
                     bottom: 5px;
                    left: 6px;
                    overflow-wrap: unset;

                    max-height:38px;
                    overflow:hidden;
              }
             
           <?php endif;?> 
        </style>              
        <div style="clear: both;"></div>
         <div style="width: auto;postion:relative" id="<?php echo $rand_Num_td;?>">
            <div id="<?php echo $rand_Numb;?>" class="responsiveSlider" style="margin-top: 2px !important;visibility: hidden;">
           <?php
                global $wpdb;
                $imageheight=$settings['imageheight'];
                $imagewidth=$settings['imagewidth'];

                if($settings['is_random']){

                      $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider where slider_id=".$settings['id']." order by rand()";
                  }
                  else{
                      $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider where slider_id=".$settings['id']." order by image_order";
                  }

              $rows=$wpdb->get_results($query,'ARRAY_A');

              if(count($rows) > 0){
                  foreach($rows as $row){

                              $imagename=$row['image_name'];
                              $imageUploadTo=$pathToImagesFolder.'/'.$imagename;
                              $imageUploadTo=str_replace("\\","/",$imageUploadTo);
                              $pathinfo=pathinfo($imageUploadTo);
                              $filenamewithoutextension=$pathinfo['filename'];
                              $outputimg="";


                              if($settings['resizeImages']==0){


                                  $outputimg = $baseurl.$row['image_name']; 

                              }
                              else{

                                      if($crop==1){
                                          $imagetoCheck=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                          $imagetoCheckLower=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.strtolower($pathinfo['extension']);
                                      }
                                      else{

                                          $imagetoCheck=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.$pathinfo['extension'];
                                          $imagetoCheckLower=$pathToImagesFolder.'/'.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.strtolower($pathinfo['extension']);

                                      }
                                      if(file_exists($imagetoCheck)){
                                         if($crop==1){  
                                              $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                         }
                                         else{

                                             $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.$pathinfo['extension'];
                                         }
                                      }
                                      else if(file_exists($imagetoCheckLower)){

                                          if($crop==1){  
                                               $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.strtolower($pathinfo['extension']);
                                          }
                                          else{
                                              $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.strtolower($pathinfo['extension']);
                                          }
                                      }
                                     else{

                                           if(function_exists('wp_get_image_editor')){

                                                  $image = wp_get_image_editor($pathToImagesFolder."/".$row['image_name']); 

                                                  $is_crop=true;
                                                  if($crop==0){

                                                      $is_crop=false;
                                                  }
                                                  if ( ! is_wp_error( $image ) ) {
                                                      $image->resize( $imagewidth, $imageheight, $is_crop );
                                                      $image->save( $imagetoCheck );
                                                      //$outputimg = plugin_dir_url(__FILE__)."imagestoscroll/".$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];

                                                       if(file_exists($imagetoCheck)){
                                                              if($crop==1){
                                                                  $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];
                                                              }
                                                              else{
                                                                  $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.$pathinfo['extension'];
                                                              }
                                                          }
                                                          else if(file_exists($imagetoCheckLower)){
                                                              if($crop==1){
                                                                  $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.strtolower($pathinfo['extension']);
                                                              }
                                                              else{
                                                                  $outputimg = $baseurl.$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'_no_crop.'.strtolower($pathinfo['extension']);
                                                              }
                                                          }
                                                  }
                                                 else{
                                                        $outputimg = $baseurl.$row['image_name'];
                                                 }     

                                            }

                                          else{

                                              $outputimg = $baseurl.$row['image_name']; 
                                          }  

                                            //$url = plugin_dir_url(__FILE__)."imagestoscroll/".$filenamewithoutextension.'_'.$imageheight.'_'.$imagewidth.'.'.$pathinfo['extension'];

                                     } 
                              } 

                              $title="";
                              $rowTitle=$row['title'];
                              $rowTitle=str_replace("'","’",$rowTitle); 
                              $rowTitle=str_replace('"','”',$rowTitle); 

                              $rowDescrption=$row['image_description'];
                              $rowDescrption=str_replace("'","’",$rowDescrption); 
                              $rowDescrption=str_replace('"','”',$rowDescrption);    

                              if(trim($rowDescrption)==''){

                                      $rowDescrption=$rowTitle;
                                  }

                   ?>         

                <?php if($crop):?>
                   <div class="v-wrap_"> 
                      <?php if($settings['linkimage']==true){ ?>                                                                                                                                                                                                                                                                                     
                        <a <?php if($row['open_link_in']==true): ?>target="_blank" <?php endif;?> <?php if($row['custom_link']!=""):?> href="<?php echo $row['custom_link'];?>" <?php endif;?> title="<?php echo $rowDescrption; ?>" ><img src="<?php echo $outputimg; ?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   /></a>
                      <?php }else{ ?>
                            <img src="<?php echo $outputimg;?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   />
                      <?php } ?> 
                   </div>
                <?php else:?>
                <div class="v-wrap" style="max-height:<?php echo $imageheight;?>px">
                      <article class="v-box" style="max-width:99.3%">
                         <?php if($settings['linkimage']==true){ ?>                                                                                                                                                                                                                                                                                     
                              <a <?php if($row['open_link_in']==true): ?>target="_blank" <?php endif;?> <?php if($row['custom_link']!=""):?> href="<?php echo $row['custom_link'];?>" <?php endif;?> title="<?php echo $rowDescrption; ?>" >
                                  <img  class="objFit center" src="<?php echo $outputimg; ?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   />
                              </a>
                            <?php }else{ ?>  
                               <img  class="objFit center" src="<?php echo $outputimg; ?>" alt="<?php echo $rowDescrption; ?>" title="<?php echo $rowTitle; ?>"   />
                            <?php } ?>    
                      </article>
                  </div>
               <?php endif;?>
             <?php }?>   
        <?php }?>   
      </div>
   </div>                
   <script>
        var $n = jQuery.noConflict();  
        $n(document).ready(function(){
             var <?php echo $rand_var_name;?>=$n('#<?php echo $rand_Num_td;?>').html();   
             var <?php echo $rand_var_name;?>_slider=$n('#<?php echo $rand_Numb;?>').bxSlider({
                 <?php if($settings['visible']==1 and $settings['min_visible']==1):?>
                   mode:'fade',
                 <?php endif;?>
                   slideWidth: <?php echo $settings['imagewidth'];?>,
                    minSlides: <?php echo $settings['min_visible'];?>,
                    maxSlides: <?php echo $settings['visible'];?>,
                    moveSlides: <?php echo $settings['scroll'];?>,
                    touchEnabled:false,
                    slideMargin:<?php echo $settings['imageMargin'];?>,  
                    speed:<?php echo $settings['speed']; ?>,
                    pause:<?php echo $settings['pause']; ?>,
                    <?php if($settings['pauseonmouseover'] and ($settings['auto']==1 or $settings['auto']==2) ){ ?>
                      autoHover: true,
                    <?php }else{ if($settings['auto']==1 or $settings['auto']==2){?>   
                      autoHover:false,
                    <?php }} ?>
                    <?php if($settings['auto']==1):?>
                     controls:false,
                    <?php else: ?>
                      controls:true,
                    <?php endif;?>
                    pager:false,
                    useCSS:false,
                    <?php if($settings['auto']==1 or $settings['auto']==2):?>
                     autoStart:true,
                     autoDelay:200,
                     auto:true,       
                    <?php endif;?>
                    <?php if($settings['circular']):?> 
                     infiniteLoop: true,
                    <?php else: ?>
                      infiniteLoop: false,
                    <?php endif;?>
                    <?php if($settings['show_caption']):?>
                      captions:true,  
                    <?php else:?>
                      captions:false,
                    <?php endif;?>
                    <?php if($settings['show_pager']):?>
                      pager:true,  
                    <?php else:?>
                      pager:false,
                    <?php endif;?>
                    easing: '<?php echo ($settings['easing']); ?>',
                    onSliderLoad: function(){
                        $n("#<?php echo $rand_Numb;?>").css('visibility','visible');
                        <?php if($crop==0):?>

                            //$n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height($n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height()+15);

                            $n("#<?php echo $rand_Numb;?> div.v-wrap ").height($n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height());
                        <?php endif;?>    
                    }

              });




                <?php if($crop==0):?>
                      var timer;
                      var width = $n(window).width();
                        $n(window).bind('resize', function(){
                            if($n(window).width() != width){

                                $n("#<?php echo $rand_Numb;?> div.v-wrap ").height('auto');
                                width = $n(window).width();
                                timer && clearTimeout(timer);
                                timer = setTimeout(onResize<?php echo $newrand;?>, 1200);

                            }   
                        });
                  <?php endif;?>     


                   function onResize<?php echo $newrand;?>(){
                                
                                        var currentslide=<?php echo $rand_var_name;?>_slider.getCurrentSlide();

                                      // <?php echo $rand_var_name;?>_slider.reloadSlider();

                                      

                                       setTimeout(function(){ 

                                            $n("#<?php echo $rand_Numb;?> div.v-wrap ").height($n("#<?php echo $rand_Num_td;?> .bx-wrapper .bx-viewport").height());
                                          //  <?php echo $rand_var_name;?>_slider.goToSlide(currentslide);


                                        }, 500);
                                        
                                      


                      }



        });         

         </script><!-- end print_responsive_thumbnail_slider_func --><?php
       $output = ob_get_clean();
       return $output;
 }   
 
 function responsive_thumbnail_slider_add_featured_image_instruction( $content ) {
        
         global $post;
         global $wpdb;
         $post_id= isset($post->ID)?$post->ID:0;
         $postThumbnailID = get_post_thumbnail_id( $post_id );
         $photoMeta = wp_get_attachment_metadata( $postThumbnailID );
         $sliderId=0;
         $checked=false;
         if(is_array($photoMeta)){
             $fileName=$photoMeta['file'];
             $pathArr=pathinfo($fileName);
             $OnlineFileName=$pathArr['basename'];
            
             $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE image_name='$OnlineFileName' and post_id=$post_id";
             $myrow  = $wpdb->get_row($query);
             $checked=false;    
             $sliderId=0;
             if(is_object($myrow)){
               $checked=true;  
               $sliderId=(int)$myrow->slider_id;
             }       
         } 
          $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider_settings order by createdon desc";
          $rows=$wpdb->get_results($query,'ARRAY_A');
         
            $content.='<p><select onchange="checkSliderCheckbox2(this);" name="thumbnail_sliderId2" id="thumbnail_sliderId2">
            <option value="" >Select Slider</option>';
                foreach($rows as $row){
                   $content.='<option ';
                   if($sliderId==$row['id']){
                       $content.=' selected="selected" '; 
                       }
                   $content.=' value="'.$row['id'].'">'.$row['name'].'</option>';
                }
            $content.='</select></p>';
            
            $content .= '<p><input type="checkbox" onclick="chceckSliderStatus2(this);" name="setasscroller2"';
             if($checked)
              $content .= 'checked="checked" ';
              
            $content .= 'id="setasscroller2" value="true"/>&nbsp;<b>Set This Images In Responsive Thumbnail Slider</b></p>';        
            
            $content .='<script>';
            $content .='function chceckSliderStatus2(thisElement){
               if(thisElement.checked){
                  
                  if(document.getElementById("thumbnail_sliderId2").value==""){
                     alert("Please selected slider");
                     document.getElementById("thumbnail_sliderId2").focus();
                     thisElement.checked=false;
                  }
               }
            }
            
            function checkSliderCheckbox2(SelectBoxElement){
            
               if(SelectBoxElement.value==""){
               
                  document.getElementById("setasscroller2").checked=false;
               } 
            
            } 
            
            ';
            $content .='</script>';
             
         return $content;
    }
   function responsive_thumbnail_slider_featured_image_save($post_id){
        
           global $post;
           global $wpdb;
           
            $uploads = wp_upload_dir();
            $baseurl=$uploads['baseurl'];
            $baseurl.='/wp-responsive-images-thumbnail-slider/';
            $baseDir=$uploads['basedir'];
            $baseDir=str_replace("\\","/",$baseDir);
            $pathToImagesFolder=$baseDir.'/wp-responsive-images-thumbnail-slider';

           if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
              return $post_id;
                
           if(isset($_POST['setasscroller2']) and isset($_POST['thumbnail_sliderId2'])){
               
               if (has_post_thumbnail( $post_id ) ){
                   
                   $postThumbnailID = get_post_thumbnail_id( $post_id );
                   //$src=wp_get_attachment_image_src( $postThumbnailID, 'large');
                   $photoMeta = wp_get_attachment_metadata( $postThumbnailID );
                   if(is_array($photoMeta) and isset($photoMeta['file'])) {
                       
                           $fileName=$photoMeta['file'];
                           $phyPath=ABSPATH;
                           $phyPath=str_replace("\\","/",$phyPath);
                           
                           $pathArray=pathinfo($fileName);
                           
                           $OnlineFileName=$pathArray['basename'];
                           $imagename_=$pathArray['filename'];
                           $file_ext=$pathArray['extension'];
                           $imagename=$imagename_.uniqid().".".$file_ext;
                            $upload_dir_n = wp_upload_dir(); 
                            $upload_dir_n=$upload_dir_n['basedir'];
                            $fileUrl=$upload_dir_n.'/'.$fileName;
                            $fileUrl=str_replace("\\","/",$fileUrl);
                           
                           $wpcurrentdir=dirname(__FILE__);
                           $wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
                           $imageUploadTo=$pathToImagesFolder.'/'.$imagename;
                           
                           
                                   
                           @copy($fileUrl, $imageUploadTo);
                           $permalink=get_permalink($post_id);
                           global $wpdb;
                           
                           $title_img=$photoMeta['image_meta']['caption'];
                           if($title_img=="")
                             $title_img= get_the_title($post_id);
                           
                           $title_img=htmlentities(strip_tags($title_img),ENT_QUOTES);
                             
                           $createdOn=date('Y-m-d h:i:s'); 
                           if(function_exists('date_i18n')){
                            
                            $createdOn=date_i18n('Y-m-d'.' '.get_option('time_format') ,false,false);
                            if(get_option('time_format')=='H:i')
                                $createdOn=date('Y-m-d H:i:s',strtotime($createdOn));
                            else   
                                $createdOn=date('Y-m-d h:i:s',strtotime($createdOn));
                          }
                           
                           $thumbnail_sliderId=(int)$_POST['thumbnail_sliderId2'];
                           $open_link_in=1;
                           $image_order=0;
                           $thumbnail_sliderId=(int)$_POST['thumbnail_sliderId2'];
                           
                           $image_description=htmlentities(strip_tags(get_excerpt_for_responsive_thumbnail_slider($post_id),ENT_QUOTES));
                           
                           $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE  post_id=$post_id and slider_id=$thumbnail_sliderId";
                           $myrow  = $wpdb->get_row($query);
                           if(is_object($myrow)){
                             @$imgOrer=(int)$myrow->image_order;  
                           }  
                           
                           if(!is_object($myrow)){
                               
                                $orderQ = "select max(image_order) as maxImgOrder from ".$wpdb->prefix."responsive_thumbnail_slider where slider_id=$thumbnail_sliderId";
                                $myrow  = $wpdb->get_row($orderQ);

                                if(is_object($myrow) and isset($myrow->maxImgOrder) and $myrow->maxImgOrder>0){

                                     $image_order=$myrow->maxImgOrder+1;
                                }
                                else{
                                     $image_order=0;
                                } 
                                $query = "INSERT INTO ".$wpdb->prefix."responsive_thumbnail_slider (title, image_name,createdon,custom_link,image_order,open_link_in,image_description,slider_id,post_id) 
                                                  VALUES ('$title_img','$imagename','$createdOn','$permalink',$image_order,$open_link_in,'$image_description',$thumbnail_sliderId,$post_id)";
                           
                           }
                           else{
                               
                                $query = "UPDATE ".$wpdb->prefix."responsive_thumbnail_slider set title='$title_img',image_name='$OnlineFileName',custom_link='$permalink',image_description='$image_description' where post_id=$post_id and slider_id=$thumbnail_sliderId";
                           }
                           
                           $wpdb->query($query);
                   
                   } 
                  // ob_start();
                    //print_r( $query);
                    //$output = ob_get_clean();
                   //file_put_contents(dirname(__FILE__).'/test.txt',$query,FILE_APPEND);
                    
               }
           }
           else{
           
                     global $post;
                     global $wpdb;
                     $post_id= isset($post->ID)?$post->ID:0;
                     $postThumbnailID = get_post_thumbnail_id( $post_id );
                     $photoMeta = wp_get_attachment_metadata( $postThumbnailID );
                     if(is_array($photoMeta) and isset($photoMeta['file'])) {
                         
                         $fileName=$photoMeta['file'];
                         $pathArr=pathinfo($fileName);
                         $OnlineFileName=$pathArr['basename'];
                        
                         $query="SELECT * FROM ".$wpdb->prefix."responsive_thumbnail_slider WHERE  post_id=$post_id";
                         $myrow  = $wpdb->get_row($query);
                         $checked=false;        
                         if(is_object($myrow)){
                           
                                $image_name=$myrow->image_name;
                                $wpcurrentdir=dirname(__FILE__);
                                $wpcurrentdir=str_replace("\\","/",$wpcurrentdir);
                                $imagename=$_FILES["image_name"]["name"];
                                $imagetoDel=$pathToImagesFolder.'/'.$image_name;
                                @unlink($imagetoDel);
                                            
                                 $query = "delete from  ".$wpdb->prefix."responsive_thumbnail_slider where post_id=$post_id";
                                 $wpdb->query($query); 
                               
                                 
                         } 
                  }      
           
           }
        
    }
     function get_excerpt_for_responsive_thumbnail_slider($post_id){
  
        $the_post = get_post($post_id); //Gets post ID
        $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
        $excerpt_length = 50; //Sets excerpt length by word count
        $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
        $words = explode(' ', $the_excerpt, $excerpt_length + 1);
        if(count($words) > $excerpt_length) :
            array_pop($words);
            array_push($words, '');
            $the_excerpt = implode(' ', $words);
        endif;
        return $the_excerpt;     
        
        
  }
  
 function responsive_thumbnail_slider_get_wp_version() {
     
   global $wp_version;
   return $wp_version;
}
 

function responsive_thumbnail_slider_is_plugin_page() {
    
   $server_uri = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
   
   foreach (array('responsive_thumbnail_slider_image_management','responsive_thumbnail_slider') as $allowURI) {
       
       if(stristr($server_uri, $allowURI)) return true;
   }
   
   return false;
}

function responsive_thumbnail_slider_admin_scripts_init() {
    
   if(responsive_thumbnail_slider_is_plugin_page()) {
      //double check for WordPress version and function exists
      if(function_exists('wp_enqueue_media') && version_compare(responsive_thumbnail_slider_get_wp_version(), '3.5', '>=')) {
         //call for new media manager
         wp_enqueue_media();
      }
      wp_enqueue_style('media');
       wp_enqueue_style( 'wp-color-picker' );
      wp_enqueue_script( 'wp-color-picker' );
   }
} 

function writ_remove_extra_p_tags($content){

        if(strpos($content, 'print_responsive_thumbnail_slider_func')!==false){
        
            
            $pattern = "/<!-- print_responsive_thumbnail_slider_func -->(.*)<!-- end print_responsive_thumbnail_slider_func -->/Uis"; 
            $content = preg_replace_callback($pattern, function($matches) {


               $altered = str_replace("<p>","",$matches[1]);
               $altered = str_replace("</p>","",$altered);
              
                $altered=str_replace("&#038;","&",$altered);
                $altered=str_replace("&#8221;",'"',$altered);
              

              return @str_replace($matches[1], $altered, $matches[0]);
            }, $content);

              
            
        }
        
        $content = str_replace("<p><!-- print_responsive_thumbnail_slider_func -->","<!-- print_responsive_thumbnail_slider_func -->",$content);
        $content = str_replace("<!-- end print_responsive_thumbnail_slider_func --></p>","<!-- end print_responsive_thumbnail_slider_func -->",$content);
        
        
        return $content;
  }
  
  add_filter('widget_text_content', 'writ_remove_extra_p_tags', 999);
  add_filter('the_content', 'writ_remove_extra_p_tags', 999);
   
?>