<?php 

/* Template Name: soporte-online */ 


get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();
					?>

					<div class="conectividad-seccionflex clearfix soluciones">
						<img src="/wp-content/uploads/2018/04/monitorizacion-responsiv.jpg" class="imagen-responsive">
						<div class="left">
							<p class="titulo-cuadro">MONITORIZACIÓN</p>
							<p class="texto">ya sabemos lo crítico que es para un Hotel su servicio Wifi. Por ello en C.T.BELL hemos implantado las soluciones precisas a fin de <strong>MONITORIZAR EN TIEMPO REAL</strong> el conjunto de Hoteles , comprobando en todo momento los dispositivos de red, el ancho de banda existente, los usuarios conectados etc…. <strong>Si surge incidencia alguna el propio sistema la detecta y envía incidencia para su resolución</strong>. Nada más rápido y efectivo a fin de que no quede ninguna zona del Hotel sin servicio. Nada más efectivo.</p>
						</div>
						<div class="right">
							<img src="/wp-content/uploads/2018/04/monitorizacion-diag.jpg" class="imagen-noresponsive">
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class="conectividad-seccionflex2 clearfix soluciones">
						<img src="/wp-content/uploads/2018/04/intervencion-responsive.jpg" class="imagen-responsive">
						<div class="left">
							<img src="/wp-content/uploads/2018/04/intervencion-diag.jpg" class="imagen-noresponsive">
						</div>
						<div class="right">
							<p class="titulo-cuadro">SERVICIO DE
							INTERVENCION RAPIDA ON LINE  </p>
							<p class="texto">Además de lo descrito anteriormente , el resto de soluciones aportadas por C.T.BELL en un Hotel y que no sean sistemas wifi cuentan igualmente con una unidad capaz de “levantar” (resolver incidencias de forma remota) servicios de forma remota en el mismo momento de la comunicación de una incidencia por parte del Hotel garantizando a este tiempos de respuesta inéditos en resolución de averías.</p>
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class=" et_pb_row et_pb_row_4 soluciones margen-contactar">
						<div class="et_pb_column et_pb_column_1_2  et_pb_column_7 et_pb_css_mix_blend_mode_passthrough">
							<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_7">
								<div class="et_pb_text_inner">
									<p class="contacta">Para cualquier consulta no dude en contactar con nosotros</p>
								</div>
							</div> <!-- .et_pb_text -->
						</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_8 et_pb_css_mix_blend_mode_passthrough et-last-child">
						<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_">
							<a class="et_pb_button  et_pb_button_0 et_pb_module et_pb_bg_layout_light" href="/contacto/">CONTACTAR</a>
						</div>
						</div> <!-- .et_pb_column -->	
					</div>
					<?php

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
