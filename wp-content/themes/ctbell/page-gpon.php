<?php 

/* Template Name: InsGpon media */ 


get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>


<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					<div class="soluciones solucionesGpon" id="hotspot-numeros">
						<div class="container">
							<h2 class="titulo-cuadro" style="text-transform:uppercase">Principales ventajes de instalación GPON en hotel:</h2>
							
							<div class="col-lg-6 col-md-6" id="left">
								<div>
									<span>1</span>
									<p class="texto">Una sola red para todos los servicios y no una red para cada servicio.</p>
								</div>
								<div>
									<span>2</span>
									<p class="texto">Mayores y mejores servicios en televisión (IPTV): publicidad, juegos on-line, etc.</p>
								</div>
								<div>
									<span>3</span>
									<p class="texto">Mayor y mejor cobertura de wifi.</p>
								</div>
								<div>
									<span>4</span>
									<p class="texto">Mayor ancho de banda: la fibra óptica permite anchos de banda de unos 2,5 Gbps.</p>
								</div>
							</div>
							<div class="col-lg-6 col-md-6" id="right">
								<div>
									<span>5</span>
									<p class="texto">Mejor y más económico mantenimiento de las soluciones.</p>
								</div>
								<div>
									<span>6</span>
									<p class="texto">Reducción de costes en infraestructura.</p>
								</div>
								<div>
									<span>7</span>
									<p class="texto">Mayor seguridad: bajo un sistema de encriptación garantiza el cifrado de toda la información.</p>
								</div>
							</div>
						</div>	
					</div>
					<div class="sectores">
						<div class="et_pb_section soluciones et_pb_section_4 et_section_regular">
							<div class=" et_pb_row et_pb_row_8">
								<div class="et_pb_column et_pb_column_1_2  et_pb_column_11 et_pb_css_mix_blend_mode_passthrough">
									<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_9">
										<div class="et_pb_text_inner">
											<p class="contacta">Para cualquier consulta no dude en contactar con nosotros</p>
										</div>
									</div> <!-- .et_pb_text -->
								</div> <!-- .et_pb_column -->
								<div class="et_pb_column et_pb_column_1_2  et_pb_column_12 et_pb_css_mix_blend_mode_passthrough et-last-child">
									<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_">
											<a class="et_pb_button  et_pb_button_0 et_pb_module et_pb_bg_layout_light" href="/contacto/">CONTACTAR</a>
									</div>
								</div> <!-- .et_pb_column -->
							</div> <!-- .et_pb_row -->
						</div>
					</div>
                    <h3 style="text-align:center;">Soluciones Gpon para hoteles en Madrid y Alicante</h3>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
