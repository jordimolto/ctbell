<?php 

/* Template Name: Home */ 


get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>


<script>
	jQuery(document).ready(function($) {
		jQuery('.vertical-slider').unslider({
			animation: 'vertical',
			autoplay: true,
			infinite: true,
			speed: 1200,
			delay: 3000
		});
	});
</script>



<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();
					?>

					
					<div class="home-seccion3 clearfix">
						<img class="imagen-responsive" src="/wp-content/uploads/2018/04/mantenimiento-responsive.jpg">
						<div class="left">
							<p class="titulo">INGENIERÍA DE PROYECTOS</p>
							<p class="texto">Proporcionamos la mejor opción a medida según sus necesidades.</p>
							<a class="boton" href="/servicios/">VER MÁS</span></a>
						</div>
						<div class="right">
							<img src="/wp-content/uploads/2018/03/ingenieria-diag.jpg" alt="Empresa de telecomunicaciones para hoteles" class="imagen-noresponsive">
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class="home-seccion4 clearfix">
						<div class="left">
							<img src="/wp-content/uploads/2018/03/mantenimiento-diag-1.jpg" alt="Servicios de telecomunicacion hotelera" class="imagen-noresponsive">
							<img src="/wp-content/uploads/2018/04/ingenieria-responsive.jpg" class="imagen-responsive">
						</div>
						<div class="right">
							<p class="titulo">MANTENIMIENTO</p>
							<p class="texto">Tan importante como los productos o servicios es el mantenimiento posterior.</p>
							<a class="boton" href="/servicios/">VER MÁS</a>
						</div>
						<div style="clear:both;"></div>
					</div>

					<div class="home-seccion5 clearfix et_pb_section">
						<p class="titulo">CASOS DE ÉXITO</p>
						<div class="container clearfix">
							<div class="casosDeExito" id="casosDeExito">
								<div class="sliderCasos vertical-slider-wrapper">
									<img src="/wp-content/uploads/2018/04/comillas.png" class="img-Comillas"/>

									<div class="unslider">
										<div class="vertical-slider unslider-vertical">
											<a href="/casos-de-exito/" class="vermas-testimonios">VER MÁS</a>
											<ul class="unslider-wrap unslider-carousel">
												<li>
													<div class="logo-testimonio">
														<img src="/wp-content/uploads/2018/03/a0e1403c87e2dbeb9edd7088e8bd5a1c.png">
													</div>
													<div class="texto-testimonio">
														<p class="persona-cargo"><span>RAFAEL BLANQUER | </span> Director  Port Hotels</p>
														<p class="texto">Calidad y calidad; eso es lo que buscamos siempre para y ofrecemos siempre en Nuestros establecimientos. Desde hace ya más de 20 años vamos de la mano de Nuestros asesores globales en Telecomunicaciones: CTBELL...  <a href="/casos-de-exito/">Leer más</a></p>
													</div>
												</li>
												<li>
													<div class="logo-testimonio">
														<img src="/wp-content/uploads/2018/03/c550a079ef5c78f66e74633478f9b394.jpg">
													</div>
													<div class="texto-testimonio">
														<p class="persona-cargo"><span>IRIS BLANCH | </span>  Directora Mediterraneo sur hoteles</p> 
														<p class="texto">Desde hace más de 20 años confiamos todas nuestras soluciones de comunicación para todos Nuestros Hoteles en C.T.Bell. Les considero grandes profesionales que detectan nuestras necesidades antes incluso que Nosotros mismos y aportan las soluciones...<a href="/casos-de-exito/">Leer más</a></p>
													</div>
												</li>
												<li>
													<div class="logo-testimonio">
														<img src="/wp-content/uploads/2018/03/6507aced5d24a9be7da9eb617639f568.png">
													</div>
													<div class="texto-testimonio">
														<p class="persona-cargo"><span>ANTONIO ESCOBAR |</span> Director Hotel Melia Benidorm</p> 
														<p class="texto">Nosotros éramos clientes de otras empresas y operadores cuando C.T.Bell nos planteó la solución que realmente satisfacía nuestras necesidades globales aportando además un gran ahorro económico. Nos pusimos en sus manos...<a href="/casos-de-exito/">Leer más</a></p>
													</div>
												</li>
												<li>
													<div class="logo-testimonio">
														<img src="/wp-content/uploads/2018/03/348197a3871be16fc23cb3ac716e2949.png">
													</div>
													<div class="texto-testimonio">
														<p class="persona-cargo"><span>JUAN JOSE FUSTER |</span>  Gerente Hotel Madeira centro</p>
														<p class="texto">Nuestra experiencia es muy satisfactoria con las instalaciones Wifi y de Centralita que efectuaron en Nuestros Hoteles. Voz IP con máxima calidad y una drástica reducción de costes y un servicio técnico que resuelve las incidencias con gran celeridad. Recientemente adquirida su solución APP...<a href="/casos-de-exito/">Leer más</a></p>
													</div>
												</li>
												<li>
													<div class="logo-testimonio">
														<img src="/wp-content/uploads/2018/04/logo_primaverapark_web.png">
													</div>
													<div class="texto-testimonio">
														<p class="persona-cargo"><span>SARA MAYOR |</span>  Dirección Primavera Park</p>
														<p class="texto">Ya son muchos años equipando Nuestros sistemas de comunicación con C.T.Bell. Recientemente hemos querido implementar las últimas tecnologías y soluciones posibles para beneficio de Nuestros Clientes , motivo por el cual recurrimos una vez más a proyecto de ingeniería de CTBELL. Nos proyectaron...<a href="/casos-de-exito/">Leer más</a></p>
													</div>
												</li>
											</ul>
										</div>
									</div>						
								</div>
							</div>
						</div>
					</div>
					<?php

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<script src="\wp-content\themes\ctbell\sliderm\unslider.js"></script> <!-- but with the right path! -->
<link rel="stylesheet" href="\wp-content\themes\ctbell\sliderm\unslider.css">
<script src="\wp-content\themes\ctbell\js\parallax.js"></script>


<?php

get_footer();

