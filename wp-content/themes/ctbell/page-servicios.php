<?php 

/* Template Name: servicios */ 


get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();
					?>

					<div class="conectividad-seccionflex clearfix soluciones">
						<img src="/wp-content/uploads/2018/04/ingenieria-responsive.jpg" class="imagen-responsive">
						<div class="left">
							<p class="titulo-cuadro">INGENIERÍA DE PROYECTOS</p>
							<p class="texto">Tanto en la construcción de un nuevo Hotel, como en una Reforma integral de un Hotel existente o en una solución de equipamiento parcial, Nuestro departamento de Ingeniería dispone de la máxima experiencia de producto así como de las necesidades de un Hotel a fin de poder prescribir las mejores soluciones a medida de cada necesidad. Dicho proyecto lo es sin coste económico cuando es adjudicada la obra de implantación.</p>
						</div>
						<div class="right">
							<img src="/wp-content/uploads/2018/04/ingenieria-diag.jpg" class="imagen-noresponsive">
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class="conectividad-seccionflex2 clearfix soluciones">
						<img src="/wp-content/uploads/2018/04/mantenimiento-responsive.jpg" class="imagen-responsive">
						<div class="left">
							<img src="/wp-content/uploads/2018/04/mantenimiento-diag.jpg" class="imagen-noresponsive">
						</div>
						<div class="right">
							<p class="titulo-cuadro">MANTENIMIENTO</p>
							<p class="texto">A día de hoy es casi tan importante que los productos y soluciones en general instaladas su mantenimiento posterior, ya que como sabemos para un Hotel es crítico que cualesquiera de esas soluciones no tengan incidencias pero si las tienen éstas sean resueltas en unos plazos cortísimos de tiempo y en cualquier hora y día. Un Hotel no cierra. Por ello y sensibles a ello C.T.BELL aporta la solución más adecuada en este tan importante apartado: <strong>SERVICIO TECNICO LAS 24 HORAS DE LOS 365 DIAS DEL AÑO</strong>. Pero además y a diferencia importante de otras Empresas, este servicio no solo se presta en su modalidad “a distancia” (por teléfono) sino <strong>PRESENCIALMENTE</strong> con técnicos operativos a cualquier hora de cualquier día si la resolución a una posible incidencia así lo requiere. Su Hotel no podrá estar en mejores manos.</p>
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class=" et_pb_row et_pb_row_4 soluciones" id="servicios-contacto">
						<div class="et_pb_column et_pb_column_1_2  et_pb_column_7 et_pb_css_mix_blend_mode_passthrough">
							<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_7">
								<div class="et_pb_text_inner">
									<p class="contacta">Para cualquier consulta no dude en contactar con nosotros</p>
								</div>
							</div> <!-- .et_pb_text -->
						</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_8 et_pb_css_mix_blend_mode_passthrough et-last-child">
						<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_">
							<a class="et_pb_button  et_pb_button_0 et_pb_module et_pb_bg_layout_light" href="/contacto/">CONTACTAR</a>
						</div>
						</div> <!-- .et_pb_column -->	
					</div>
					<?php

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
