<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>
		<div id="newsletter-footer" class="soluciones clearfix">
			<div class="container">
				<div class="col-lg-6 col-md-6">
					<p class="titulo-cuadro2">SUSCRÍBASE A<br> 
					NUESTRAS PROMOCIONES</p>
				</div>
				<div class="col-lg-6 col-md-6">
					<form action="https://ctbell.us18.list-manage.com/subscribe/post?u=0f77ec2f713c1524c1875e293&amp;id=a165f77c2f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate formulario-newsletter" target="_blank" novalidate>
					    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Su email*" required>
					    <div class="chekbox-div">
					    	<input type="checkbox" class="checkbox-newsletter"> <p>He leído y acepto la <a href="/politica-de-privacidad">Política de Privacidad</a></p>
				    	</div>
					    
					    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0f77ec2f713c1524c1875e293_a165f77c2f" tabindex="-1" value=""></div>
					    <input type="submit" value="SUSCRIBIRME" name="subscribe" id="mc-embedded-subscribe" class="button">
					    </div>
					</form>
				</div>
				
			</div>
		</div>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>

<script>
	jQuery(document).ready(function(){
		jQuery('.formulario-newsletter').submit(function(e){
			
			if( jQuery('.checkbox-newsletter').prop('checked')){
				return true;
			}else{
				alert('Tienes que aceptar la politica de privacidad');
			}
			e.preventDefault();
			return false;


		});
	});
	

</script>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#eaf7f7",
      "text": "#5c7291"
    },
    "button": {
      "background": "#56cbdb",
      "text": "#ffffff"
    }
  },
  "theme": "edgeless",
  "content": {
    "message": "Este sitio web utiliza cookies para garantizar la mejor experiencia en nuestra página. ",
    "dismiss": "Acepto",
    "link": "Leer más",
    "href": "/politica-de-cookies/"
  }
})});
</script>