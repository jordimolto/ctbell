<?php 

/* Template Name: callcenter */ 


get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<script>
	jQuery(function(){
	  jQuery("#prestaciones a").click(function(e){
	           
	        e.preventDefault();

	        jQuery('.selector-acordeon').removeClass('active').addClass('no-active');
	    	
	        var contenido=jQuery(this).next(".texto-acordeon");
	        var ocultar=jQuery('.texto-acordeon');
	        if(contenido.css("display")=="none"){ //open    
	          ocultar.slideUp(500);    
	          contenido.slideDown(500);         
	          jQuery(this).addClass("active");
	          jQuery(this).removeClass("no-active");
	        }
	        else{ //close       
	          contenido.slideUp(500);
	          jQuery(this).removeClass("active");  
	          jQuery(this).addClass("no-active");  
	        }

	      });
	});
</script>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					<div class="soluciones container" id="acordeon">
						<p class="titulo-cuadro">PRESTACIONES OPCIONALES</p>
						<p class="texto">Además de los módulos estándar, está disponible un completo catálogo de características y funciones opcionales para adaptarse perfectamente a las necesidades y requisitos específicos de cada cliente.
						</p>
						<div class="col-lg-6 col-md-6 margin-top">
							<div id="prestaciones" class="prestaciones-1">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>1</span> Módulo Dashboard</a>
								<p class="texto-acordeon">El módulo Wallboard muestra la información más relevante de la actividad del Call Center y de la calidad de servicio.  Es una potente aplicación accesible vía web  y adaptable a pantallas murales o de pared, estaciones de supervisor y teléfonos móviles. Los datos estadísticos se muestran y se actualizan en tiempo real. </p>
							</div>
							<div id="prestaciones" class="prestaciones-2">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>2</span> Integración CRM</a>
								<p class="texto-acordeon">Se puede integrar también con software CRMs, ERPs y aplicaciones internas. Esta integración puede usarse para identificar el cliente y transferir la llamada al agente más adecuado o presentar automáticamente en el monitor del agente la ficha del cliente cuando se recibe la llamada.</p>
							</div>
							<div id="prestaciones" class="prestaciones-3">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>3</span> Encuesta Post-llamada</a>
								<p class="texto-acordeon">Esta opción automatiza la realización de encuestas al final de la llamada para medir la calidad de servicio y el nivel de satisfacción. Los resultados son archivados junto con los datos del contacto y del agente que atendió la llamada.</p>
							</div>
							<div id="prestaciones" class="prestaciones-4">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>4</span> Grabación de llamadas</a>
								<p class="texto-acordeon">La solución ACD ofrece una amplia gama de opciones de grabación para todo tipo de líneas y extensiones. Grabación permanente de todas las llamadas, grabación de solo algunas llamadas según el agente, grupo o cliente y grabación bajo petición del agente o del supervisor.</p>
							</div>
							<div id="prestaciones" class="prestaciones-5">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>5</span> Telemarketing</a>
								<p class="texto-acordeon">Diseñado para la realización de campañas de llamadas salientes usando modos de marcación automática (Vista Previa, Progresiva y Predictiva) y el procesamiento de formularios inteligentes. 
								Fidelity Telemarketing genera un gran número de llamadas a clientes y contactos potenciales desde una base de datos existente.</p>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div id="prestaciones" class="prestaciones-6 margin-top">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>6</span> Distribución de E-mails</a>
								<p class="texto-acordeon">Permite la distribución inteligente de e-mails a diferentes grupos de agentes en función de palabras clave identificadas en el asunto o en el cuerpo del e-mail.
								Esta función se puede combinar con la distribución de llamadas para que un agente atienda al mismo tiempo llamadas y e-mails.</p>
							</div>
							<div id="prestaciones" class="prestaciones-7">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>7</span> Módulo Chat</a>
								<p class="texto-acordeon">Permite a los clientes contactar con un agente y dialogar en tiempo real (modo chat) desde la página web que están consultando.
								Este módulo abre una nueva y cómoda forma de comunicación cliente-agente.</p>
							</div>
							<div id="prestaciones" class="prestaciones-8">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>8</span> Call Back desde la Web</a>
								<p class="texto-acordeon">Permite a las personas solicitar desde la página web que están visitando, solicitar una rellamada automática por parte de un agente presionando el botón "call-me-back" de la web e introduciendo su número de teléfono.</p>
							</div>
							<div id="prestaciones" class="prestaciones-9">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>9</span> Call Back Automático</a>
								<p class="texto-acordeon">Ofrece a la persona que llama la posibilidad de salirse de la cola de espera y de solicitar una devolución de la llamada. 
								Mientras espera en la cola, un mensaje informa a la persona que llama sobre la posibilidad de dejar de esperar y ser llamado por el primer agente que quede disponible. 
								Además, todas las llamadas perdidas  son rellamadas automáticamente</p>
							</div>
							<div id="prestaciones" class="prestaciones-10">
								<a href="#" id="selector-acordeon" class="selector-acordeon no-active"><span>10</span> Click to Call</a>
								<p class="texto-acordeon">La opción Click to Call permite a los usuarios  realizar llamadas salientes desde el formulario CRM que aparece en pantalla simplemente con solo pulsar el botón de llamada.
								Esta opción se entrega como un servicio web que se invoca* por un botón programado en el CRM.</p>
							</div>
						</div>	
					</div>
					<div class="sectores">
						<div class="et_pb_section soluciones et_pb_section_4 et_section_regular">
							<div class=" et_pb_row et_pb_row_8">
								<div class="et_pb_column et_pb_column_1_2  et_pb_column_11 et_pb_css_mix_blend_mode_passthrough">
									<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_9">
										<div class="et_pb_text_inner">
											<p class="contacta">Para cualquier consulta no dude en contactar con nosotros</p>
										</div>
									</div> <!-- .et_pb_text -->
								</div> <!-- .et_pb_column -->
								<div class="et_pb_column et_pb_column_1_2  et_pb_column_12 et_pb_css_mix_blend_mode_passthrough et-last-child">
									<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_">
											<a class="et_pb_button  et_pb_button_0 et_pb_module et_pb_bg_layout_light" href="/contacto/">CONTACTAR</a>
									</div>
								</div> <!-- .et_pb_column -->
							</div> <!-- .et_pb_row -->
						</div>
					</div>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
