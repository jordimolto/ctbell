<?php 

/* Template Name: conectividad */ 


get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();
					?>

					<div class="conectividad-seccionflex clearfix soluciones">
						<img src="/wp-content/uploads/2018/04/fibra-responsiv.jpg" class="imagen-responsive">
						<div class="left">
							<p class="titulo-cuadro">FIBRA ÓPTICA</p>
							<p class="texto">Para ofrecer la solución más adecuada a cada necesidad ofrecemos dos tipos de conectividad de Fibra:</p>
							<br>
							<div style="position:relative;" class="clearfix">
								<span class="numero-prestaciones">1</span>
								<p class="subtitulo-cuadro">Fibra Pro 300 Mb</p>
								<p class="texto2">300 Mb simétricos y 1 IP fija</p>
							</div>
							<br>
							<div style="position:relative;" class="clearfix">
								<span class="numero-prestaciones">2</span>
								<p class="subtitulo-cuadro">Fibra Premium 300 Mb</p>
								<p class="texto2">300 Mb simétricos + /30 Rango de 4 IPs (2 útiles)</p>
							</div>
							<br>
							<p class="texto">En ambos casos incluye Router y ONT
							*Consultar cobertura de Fibra para ver disponibilidad</p>
						</div>
						<div class="right">
							<img  class="imagen-noresponsive" src="/wp-content/uploads/2018/04/fibra-diagonal.jpg">
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class="conectividad-seccionflex2 clearfix soluciones">
							<img class="imagen-responsive" src="/wp-content/uploads/2018/04/wimax-responsiv.jpg">
						<div class="left">
							<img  class="imagen-noresponsive" src="/wp-content/uploads/2018/04/wimax-diagonal.jpg">
						</div>
						<div class="right">
							<p class="titulo-cuadro">WIMAX</p>
							<p class="texto">Solución de <strong>conectividad inalámbrica de largo alcance</strong> por la cual podemos equipar un Hotel con grandes capacidades de caudal para sus necesidades de voz, datos y video vía Radio. Es la <strong>solución idónea para aquellas ubicaciones que no dispongan de Fibra Optica.</strong></p>
						</div>
						<div style="clear:both;"></div>
					</div>
					<div class="et_pb_section soluciones et_pb_section_3 et_section_regular">
						<div class=" et_pb_row et_pb_row_6">
							<div class="et_pb_column et_pb_column_1_2  et_pb_column_16 et_pb_css_mix_blend_mode_passthrough">
								<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_13">
									<div class="et_pb_text_inner">
										<p class="contacta">Para cualquier consulta no dude en contactar con nosotros</p>
									</div>
								</div> <!-- .et_pb_text -->
							</div> <!-- .et_pb_column -->
							<div class="et_pb_column et_pb_column_1_2  et_pb_column_17 et_pb_css_mix_blend_mode_passthrough et-last-child">

								<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_">
									<a class="et_pb_button  et_pb_button_0 et_pb_module et_pb_bg_layout_light" href="/contacto/">CONTACTAR</a>
								</div>
							</div> <!-- .et_pb_column -->
						</div> <!-- .et_pb_row -->
					</div>
					<?php

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
