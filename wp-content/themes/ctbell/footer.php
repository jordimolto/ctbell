<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>
	<div id="sectContact" class="soluciones clearfix">
		<div class="container">
			<div class="col-lg-12 col-md-12">
				<div class="titleContact">¿Hablamos?</div>
				<div class="subtitleContact">No dudes en contactar con nosotros para cualquier consulta</div>
				<div class="ctaContact"><a href="#formContact">Contactar</a></div>
			</div>
		</div>
	</div>	
		<div id="newsletter-footer" class="soluciones clearfix">
			<div class="container">
				<div class="col-lg-6 col-md-6">
					<p class="titulo-cuadro2">SUSCRÍBASE A<br> 
					NUESTRAS PROMOCIONES</p>
				</div>
				<div class="col-lg-6 col-md-6">
					<form action="https://ctbell.us18.list-manage.com/subscribe/post?u=0f77ec2f713c1524c1875e293&amp;id=a165f77c2f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate formulario-newsletter" target="_blank" novalidate>
					    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Su email*" required>
					    <div class="chekbox-div">
					    	<input type="checkbox" class="checkbox-newsletter"> <p>He leído y acepto la <a href="/politica-de-privacidad">Política de Privacidad y los Permisos de promoción</a></p>
				    	</div>
					    
					    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0f77ec2f713c1524c1875e293_a165f77c2f" tabindex="-1" value=""></div>
					    <input type="submit" value="SUSCRIBIRME" name="subscribe" id="mc-embedded-subscribe" class="button">
					    </div>
					</form>
				</div>
				
			</div>
		</div>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
	<div id="formContacSlide">
		<div class="closeFrom"><img src="/wp-content/uploads/close.svg"></div>
		<div class="titleFormSlide">
			¿Hablamos?
		</div>
		<div class="bodyFormSlide"><p>Estaremos encantados de poder atenderte con cualquier consulta relacionada.</p></div>
		<div id="formContactSlide">
			<?php echo do_shortcode('[contact-form-7 id="555" title="Formulario Contacto Lateral"]'); ?>
		</div>
	</div>
	<div id="ctaFixed">
		<a href="#formContact">Contactar</a>
	</div>
</body>
</html>

<script>
	jQuery(document).ready(function(){
		jQuery('.formulario-newsletter').submit(function(e){
			
			if( jQuery('.checkbox-newsletter').prop('checked')){
				return true;
			}else{
				alert('Tienes que aceptar la politica de privacidad');
			}
			e.preventDefault();
			return false;
		});
		jQuery(".ctaContact a").click(function(e) {  
			event.preventDefault();
		    jQuery("#formContacSlide").addClass("opened");      //add the class to the clicked element
		 });
		jQuery("#ctaFixed a").click(function(e) {  
			event.preventDefault();
		    jQuery("#formContacSlide").addClass("opened");      //add the class to the clicked element
		 });
		jQuery("#sliderHome .et_pb_more_button").click(function(e) {  
			event.preventDefault();
		    jQuery("#formContacSlide").addClass("opened");      //add the class to the clicked element
		 });
		jQuery(".closeFrom").click(function(e) {  
		    jQuery("#formContacSlide").removeClass("opened");      //add the class to the clicked element
		 });

	});
	

</script>

